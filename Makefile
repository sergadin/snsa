init:
	pip install -r requirements.txt

test:
	python setup.py test

docs:
	(cd docs; make html)

.PHONY: init test
