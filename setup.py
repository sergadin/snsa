﻿# -*- coding: utf-8; -*-

from setuptools import setup, find_packages
from os.path import join, dirname

import snsa

setup(
    name='snsa',
    version=snsa.__version__,
    packages=find_packages(),
    description='Social Network Sentiment Analizer',
    long_description=open("README.md", encoding="utf-8").read(), #open(join(dirname(__file__), 'README.md')).read(),
    entry_points={
        'console_scripts':
            ['snsa_collect = spider:collect_data']
    },
    include_package_data=True,
    test_suite='tests',
)
