SNSA
====

Установка
---------

Windows
-------
```bash
  pip install virtualenv
  py -3 -m virtualenv env
```
Дальше должны работать команды для Linux, если явно указывать `.\env\Scripts\python.exe`.

Linux
-----
Установка под Linux
```bash
  # Ставим необходимые системные пакеты
  sudo apt install python3 python3-pip
  sudo apt install python3-pip
  pip3 install --upgrade pip
  pip3 install virtualenv
  python3 -m  virtualenv env
  sudo apt install virtualenvwrapper

  # Создаем виртуальное окружение
  . /usr/share/virtualenvwrapper/virtualenvwrapper.sh
  mkvirtualenv snsa
  workon snsa
  cdvirtualenv

  # Скачиваем код
  git clone git@gitlab.com:sergadin/snsa.git
  cd snsa
  pip3 install -r requirements.txt

  # Начальная установка для Django
  cd snsa_frontend
  python3 manage.py migrate

  # Запускаем локальный сервер
  python3 manage.py runserver
```

После этого можно открыть страницу http://localhost:8000/
