import scrapy

class RXListSpider(scrapy.Spider):
    name = "RXList"
    start_urls = [
        'https://www.rxlist.com/drugs/alpha_a.htm', # Алфавитный указатель лекарств
    ]

    def parse(self, response):
        print("Parsing....")
        for drugs in response.css('div.contentstyle'):
            for drug_name in drugs.xpath('//ul/li/a/text()').extract():
                yield {
                    'name': drug_name,
                }

        #next_page = response.css('li.next a::attr("href")').extract_first()
        #if next_page is not None:
        #    yield response.follow(next_page, self.parse)

