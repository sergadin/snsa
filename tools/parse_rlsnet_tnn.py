import os, sys
sys.path.insert(1, os.path.join(sys.path[0], '..'))

from os import walk
from snsa import settings
from bs4 import BeautifulSoup

INSTRUCTIONS_FILE = settings.top_directory + '/local/learning-instructions.txt'

def extract_instruction(filename):
    content = ''
    with open(filename, 'r', encoding='cp1251') as file:
        content = file.read()
    soup =  BeautifulSoup(content, 'html.parser')
    instruction_div = soup.find_all('div', class_='instructiontext')
    if not instruction_div:
        return None
    instruction_div = instruction_div[0]
    paragraphs = []
    for instruction_part in instruction_div.find_all(['h3', 'div']): #, class_='WordSection1'):
        paragraphs.append(instruction_part.text.replace('\n', ' '))
    return paragraphs # instruction_div.text.replace('\n', ' ') + '\n\n'


instructions = []
count = 0
for (dirpath, dirnames, filenames) in walk(settings.top_directory + '/local/rlsnet'):
    total_files = len(filenames)
    for seq, filename in enumerate(filenames):
        if filename.startswith('tn_index_id'):
            absolute_filename = os.path.join(dirpath, filename)
            print(seq, total_files, seq/total_files, count, filename)
            instruction = extract_instruction(absolute_filename)
            if instruction is not None:
                instructions.append(instruction)
                count += 1
    print(len(filenames))
    break

print(count)
# print(instructions)

with open(INSTRUCTIONS_FILE, 'w', encoding='utf-8') as file:
    for instruction in instructions:
        for paragraph in instruction:
            file.write(paragraph + ' ')
        file.write('\n\n')

