﻿# -*- coding: utf-8; -*-

import unittest
import snsa.nlp.tokenizer as tokenizer
from snsa.nlp.features import Passage

class TestSimpleTokenization(unittest.TestCase):

    def test_punct(self):
        sentence = 'abc, x?!'
        self.assertEqual(tokenizer.tokenize(sentence), ['abc', ',', 'x', '?!'])

    def test_cyrillic(self):
        sentence = 'фыва, я овы туыв!'
        self.assertEqual(tokenizer.tokenize(sentence), ['фыва', ',', 'я', 'овы', 'туыв', '!'])

class TestSentenceSplitting(unittest.TestCase):
    def test_correct_input(self):
        text = 'Это первое предложение. А это уже второе!'
        passage = Passage(text)
        self.assertEqual(len(passage._sentences), 2)
        self.assertEqual(passage._sentences[0].length(), 4)
        self.assertEqual(passage._sentences[1].length(), 5)
