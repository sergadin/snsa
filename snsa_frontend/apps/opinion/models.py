# -*- coding: utf-8 -*-
import datetime
from django.db import models

class ProcessingTask(models.Model):
    id = models.AutoField(primary_key=True, db_column="f_proctask_id")
    drugname = models.CharField("Название препарата", max_length=255, db_column="f_proctask_drugname")
    aliases = models.CharField("Другие названия", max_length=255, db_column="f_proctask_aliases")
    status = models.CharField("Состояние", max_length=255, db_column="f_proctask_status")
    date = models.DateTimeField('Дата', db_column='f_proctask_date', auto_now_add=True)

    class Meta:
        db_table = "PROCTASK"
        verbose_name = u"Задача"
        verbose_name_plural = u"Задачи"
        ordering = ("date",)

    def __unicode__(self):
        return 'Задание для (%s)' % (self.drugname,)

class ProcessingTaskLog(models.Model):
    id = models.AutoField(primary_key=True, db_column="f_tasklog_id")
    task = models.ForeignKey(to=ProcessingTask, on_delete=models.CASCADE, related_name="old_statuses", db_column="f_proctask_id")
    status = models.CharField("Состояние", max_length=255, db_column="f_tasklog_status")
    date = models.DateTimeField('Дата', db_column='f_tasklog_date')

    class Meta:
        db_table = "TASKLOG"


class ForumPage(models.Model):
    id = models.AutoField(primary_key=True, db_column="f_forumpage_id")
    drugname = models.CharField("Препарат", max_length=255, db_column="f_forumpage_drugname")
    url = models.URLField("Адрес", max_length=512, db_column="f_forumpage_url")
    domain = models.CharField("Домен", max_length=512, db_column="f_forumpage_domain")
    title = models.CharField("Заголовок", max_length=512, db_column="f_forumpage_title")

    class Meta:
        db_table = "FORUMPAGE"


class SiteInfo(models.Model):
    id = models.AutoField(primary_key=True, db_column="f_siteinfo_id")
    drugname = models.CharField("Препарат", max_length=255, db_column="f_siteinfo_drugname")
    domain = models.CharField("Домен", max_length=512, db_column="f_siteinfo_domain")
    docscount = models.IntegerField("Число документов", db_column="f_siteinfo_docscount")

    class Meta:
        db_table = "SITEINFO"
