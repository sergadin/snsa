from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.generic import TemplateView
from django import forms
from django.shortcuts import redirect
from django.views.decorators.cache import cache_control

from .models import ProcessingTask, ForumPage, SiteInfo

import snsa.spider.yandex as yandex
import snsa.feapi as feapi
from snsa.feapi.teaching import save_snippet_evaluation
from snsa.feapi.pages import pages_for_task, page_data
import snsa.settings as snsa_settings

class NewTaskForm(forms.Form):
    drugname = forms.CharField(label='Название препарата', widget=forms.TextInput(attrs={'placeholder': 'Например, на русском'}),
                        help_text='Основное, наиболее распространенное, название.')
    aliases = forms.CharField(label='Другие названия', widget=forms.TextInput(attrs={'placeholder': 'другое; drugname; another name'}),
                        help_text='Другие названия через точку с запятой.',
                        required=False)

    def clean_drugname(self):
        drugname = self.cleaned_data.get('drugname', '')
        if ProcessingTask.objects.filter(drugname__iexact=drugname).exists():
            raise forms.ValidationError( _('Task exists: %(value)s.'),
                code='invalid',
                params={'value': drugname})
        return drugname

class TryTheBotForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea())
    snippet_id = forms.IntegerField(required=False)


class SNSATemplateView(TemplateView):
    def dispatch(self, *args, **kwargs):
        try:
            session = snsa_settings.DB.Session()
            result = super().dispatch(*args, **kwargs)
            session.commit()
            return result
        finally:
            snsa_settings.DB.Session.remove()


class IndexView(SNSATemplateView):
    template_name = "opinion/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tasks'] = ProcessingTask.objects.all().order_by('-date')
        context['newtask_form'] = NewTaskForm()
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        form = NewTaskForm(request.POST)
        if form.is_valid():
            new_task = ProcessingTask.objects.create(
                drugname=form.cleaned_data['drugname'],
                aliases=form.cleaned_data['aliases'],
                status='Created')
            return redirect('opinion_index')
        context['newtask_form'] = form
        return self.render_to_response(context, **kwargs)

class TaskInfoView(SNSATemplateView):
    template_name = "opinion/taskinfo.html"
    top_sites_limit = 25 # Сколько топовых сайтов показывать на странице

    def get_context_data(self, **kwargs):
        def annotate_page_info(pg):
            if pg['max_snippet_evaluation'] > 0:
                pg['show_positive'] = True
                pg['max_snippet_evaluation'] = int(pg['max_snippet_evaluation'])
            if pg['min_snippet_evaluation'] < 0:
                pg['show_negative'] = True
                pg['negative_class'] = 'danger' if pg['min_snippet_evaluation'] < -5 else 'warning'
                pg['min_snippet_evaluation'] = int(pg['min_snippet_evaluation'])
            return pg
        context = super().get_context_data(**kwargs)
        taskid = kwargs.get('taskid')
        task = ProcessingTask.objects.get(pk=taskid)
        pages = [annotate_page_info(pi) for pi in pages_for_task(task.id)]
        #pages = ForumPage.objects.filter(drugname__iexact=task.drugname)
        sites = SiteInfo.objects.filter(drugname__iexact=task.drugname)
        sites = sites.order_by('-docscount')
        context['pages'] = pages
        context['sites'] = sites[:self.top_sites_limit]
        context['task'] = task
        return context

class PageSnippetsView(SNSATemplateView):
    template_name = "opinion/page_snippets.html"

    def get_context_data(self, **kwargs):
        pageid = int(kwargs.get('pageid'))
        context = super().get_context_data(**kwargs)
        context['snippets'] = page_data(pageid)
        return context


class YandexLimitsAJAXView(SNSATemplateView):
    """Обрабатывает AJAX-запрос на загрузку количества оставшихся запросов к Yandex."""
    template_name = None

    @cache_control(max_age=60)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        limit = yandex.default_engine.get_limits()
        return HttpResponse("%d" % limit)


class TeachTheBotView(SNSATemplateView):
    """Показ последовательности текстовых фрагментов для формирования обучающей выборки."""
    template_name = "opinion/teach.html"


from snsa.feapi.teaching import next_snippet, get_snippet
class NextSnippetAJAXView(SNSATemplateView):
    """Возвращает фрагмент текста для экспертной оценки тональности."""
    template_name = None

    def get(self, request):
        return JsonResponse(next_snippet())

class GetSnippetAJAXView(SNSATemplateView):
    """Возвращает заданный фрагмент текста для экспертной оценки тональности."""
    template_name = None

    def get(self, request, snippet_id=None):
        from_the_request = request.GET.get('snippet_id')
        snippet = get_snippet(snippet_id or from_the_request)
        return JsonResponse({
            'text': snippet.content(),
            'snippet_id': snippet.id
        })


class TeacherFeedbackAJAXView(SNSATemplateView):
    """Обрабатывает AJAX-запрос для сохранения пользовательской оценки фрагмента."""
    template_name = None

    def get(self, request, snippet_id, decision):
        save_snippet_evaluation(snippet_id, decision)
        return HttpResponse("Saved")


## 
class TryTheBotView(SNSATemplateView):
    """Показ формы для ввода текста и проверки качества работы классификаторов."""
    template_name = "opinion/try-the-bot.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['snippets'] = feapi.teaching.snippets_to_evaluate(50)
        return context

class TryTheBotAJAXView(SNSATemplateView):
    """Обработка запроса на оценку. Полученный текст оценивается на то, что он является отзывом."""
    template_name = None

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        form = TryTheBotForm(request.POST)
        if form.is_valid():
            result = feapi.teaching.evaluate_text_for_opinion(form.cleaned_data['text'])
            print(result)
            return JsonResponse(result)
        return JsonResponse({'message': 'FAILED'})


class TeacherTrainingSetkAJAXView(SNSATemplateView):
    """Обрабатывает AJAX-запрос для сохранения пользовательской оценки элемента обучающей выборки."""
    template_name = None

    def post(self, request):
        text = request.POST.get('text')
        snippet_id = request.POST.get('snippet_id')
        review_confidence = request.POST.get('review_confidence')
        opinion_polarity = request.POST.get('opinion_polarity')

        if review_confidence is not None:
            feapi.teaching.save_training_set_mark(
                snippet_id=snippet_id,
                text=text,
                review_confidence=review_confidence,
                opinion_polarity=opinion_polarity)
        return HttpResponse("Saved")


class BrowseTrainingSetView(SNSATemplateView):
    template_name = "opinion/browse_training_set.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['trainingSetItems'] = list(feapi.teaching.get_trining_set_items())
        return context



### Tweets. TODO: split this file
from snsa.feapi.twitter import search as twitter_search
class TwitterSearchView(SNSATemplateView):
    template_name = "opinion/tweets.html"

    def post(self, request):
        terms = request.POST.get('termsToSearch')
        context = self.get_context_data()
        context['tweets'] = list(twitter_search(terms))
        return self.render_to_response(context)

    def get(self, request):
        terms = 'Pandemrix'
        context = self.get_context_data()
        context['tweets'] = list(twitter_search(terms))
        return self.render_to_response(context)
