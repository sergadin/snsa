﻿from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='opinion_index'),
    path('task/<int:taskid>/', views.TaskInfoView.as_view(), name='opinion_taskinfo'),
    path('page/snippets/<int:pageid>/', views.PageSnippetsView.as_view(), name='opinion_page_snippets'),
    path('limits/yandex/', views.YandexLimitsAJAXView.as_view(), name='opinion_limits_yandex'),
    path('teach/', views.TeachTheBotView.as_view(), name='opinion_teach'),
    path('teach/get/snippet/next/', views.NextSnippetAJAXView.as_view(), name='opinion_teach_next_snippet'),
#    path('teach/get/snippet/<int:snippet_id>/', views.GetSnippetAJAXView.as_view(), name='opinion_teach_get_snippet'),
    path('teach/get/snippet/', views.GetSnippetAJAXView.as_view(), name='opinion_teach_get_snippet'),
    path('teacher/snippet/<int:snippet_id>/<slug:decision>/', views.TeacherFeedbackAJAXView.as_view(), name='opinion_evaluate_snippet'),
    path('teach/try/', views.TryTheBotView.as_view(), name='opinion_try_the_bot'),
    path('teach/try/evaluate/', views.TryTheBotAJAXView.as_view(), name='opinion_try_the_bot_evaluate'),
    path('teach/item/save/', views.TeacherTrainingSetkAJAXView.as_view(), name='opinion_teach_item_save'),
    #
    path('teach/trainingset/browse/', views.BrowseTrainingSetView.as_view(), name='opinion_teach_training__set_browse'),
    #
    path('twitter/search/', views.TwitterSearchView.as_view(), name='opinion_twitter_search'),
]
