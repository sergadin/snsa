import scrapy
from scrapy.linkextractors import LinkExtractor

#import snsa.settings as settings

class RLSNetSpider(scrapy.Spider):
    index_section_prefix = None

    download_delay = 0.2 # Randomized delay (seconds)

    custom_settings = {
        'USER_AGENT': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
        # Cache
        'HTTPCACHE_ENABLED': True,
        'HTTPCACHE_DIR': 'local/scrapy.cache/',
        'HTTPCACHE_STORAGE': 'scrapy.extensions.httpcache.FilesystemCacheStorage',
        'HTTPCACHE_POLICY': 'scrapy.extensions.httpcache.DummyPolicy',
        'HTTPCACHE_IGNORE_HTTP_CODES': [403],
        'HTTPCACHE_ALWAYS_STORE': True,
        'HTTPCACHE_IGNORE_MISSING': False, # If enabled, requests not found in the cache will be ignored instead of downloaded.
    }

    def __init__(self):
        self.content_page_url_re = 'https://www.rlsnet.ru/{pref}_index_id_[0-9]+.htm'.format(pref=self.index_section_prefix)
        self.content_link_extractor = LinkExtractor(allow=self.content_page_url_re)

    def start_requests(self):
        '''Генератор списка индексных страниц в своей категории (tn, mnn и т.п.).'''
        page_codes = ['{:02x}'.format(code) for code in range(0xc0, 0xdf + 1)] # названия на русском
        page_codes.extend([chr(i) for i in range(ord('a'), ord('z')+1)]) # латинские названия
        page_codes.extend(['1', '2']) # цифры (первый символ)
        url_template = 'https://www.rlsnet.ru/{pref}_alf_letter_{pc}.htm'
        for code in page_codes:
            yield scrapy.Request(url=url_template.format(pc=code, pref=self.index_section_prefix), callback=self.parse_index_page)

    def parse_index_page(self, response):
        self.save_content(response)

        for contlink in self.content_link_extractor.extract_links(response):
            yield response.follow(contlink, self.parse_content_page)

    def parse_content_page(self, response):
        self.save_content(response)

    def save_content(self, response):
        # Сохраняем полный текст страницы
        ## filename = settings.top_directory + '/local/rlsnet/' + response.url.split("/")[-1]
        filename = 'local/rlsnet/' + response.url.split("/")[-1]
        with open(filename, 'wb') as f:
            f.write(response.body)


class RLSNetMNNSpider(RLSNetSpider):
    '''Индексирование страниц действующих веществ.'''
    name = 'rlsnet_mnn'
    index_section_prefix = 'mnn'

    def parse_content_page(self, response):
        def get_section(div_id):
            expr = '//div[starts-with(@id, "{id}")]/following-sibling::h2[1]/following-sibling::text()[1]'.format(id=div_id)
            return response.selector.xpath(expr).extract_first()        

        print(response.url.split("/")[-1], get_section('russkoe-nazvanie'), ' === ', get_section('latinskoe-nazvanie'))

# class RLSNetTNSpider(RLSNetSpider):
#     '''Индексирование страниц с торговыми названиями.'''
#     name = "rlsnet_tn"
#     index_section_prefix = 'tn'
