# -*- coding: utf-8; -*-
'''
    Поиск документов в Яндекс.
'''
from typing import List
from collections import Counter
import urllib.request
import urllib.parse
import xml.etree.ElementTree as ET
from xml.sax.saxutils import escape as xmlescape
from snsa import settings

YANDEX_ERRORS = {
    1: "The query parameter contains syntactic error.",
    2: "Empty query submitted.",
    15: "No results found for the query.",
    18: "XML query can not be validated (tags, unescaped ampersands, invalid pagination, etc).",
    19: "The query contains inconsistent constraints, e.g., grouping.",
    20: "Contact support if the error persists.",
    31: "The user is not registered at Yandex.XML.",
    32: "Daily limit of allowed queries reached.",
    33: "The IP-address the query was submitted from does not match a registered address(-es).",
    34: "The user is not registered at Yandex.passport.",
    37: "Invalid query parameters, e.g. mandatory ones are missing.",
    42: "The application key contains an error, check the key.",
    43: "Key's version contains an error, check the key.",
    44: "The endpoint address is not supported, update it.",
    48: "The application is not registered for this type of search.",
    55: "Requests per second limit exceeded.",
    100: "The request is likely to be submitted by a robot. CAPTCHA should be presented.",
}

MAX_DOCS_PER_REQUEST = 100 # Yandex predefined limit on number of documents/groups on one page.
MAX_DOCUMENTS_PER_QUERY = 1000 # Максимальное число документов по запросу.

class YandexException(Exception):
    def __init__(self, code, message):
        self.code = code
        try:
            self.description = YANDEX_ERRORS[int(code)]
        except (TypeError, KeyError): 
            self.description = 'No description available'
        self.message = message

    def __str__(self):
        return 'Code: %s [%s]; %s' % (self.code, self.description, self.message)

class MatchingDocument:
    """Информация о документе со страницы выдачи Яндекса."""
    def __init__(self, url: str, domain: str, title: str, charset: str, modtime: str, snippets: List[str], id=None):
        self.id = id
        self.url = url
        self.domain = domain
        self.title = title
        self.charset = charset
        self.modtime = modtime
        self.snippets = snippets


class Result:
    """Результат выполнения запроса."""

    def __init__(self):
        self.documents = []
        self.sites_statistics = Counter()
        self._ndocs_expected = None

    def add_doc(self, doc: MatchingDocument):
        self.documents.append(doc)

    def add_siteinfo(self, site_name, docs_count):
        self.sites_statistics[site_name] = docs_count

    def ndocs_fetched(self):
        return len(self.documents)

    def ndocs_expected(self):
        return self._ndocs_expected

    def __str__(self):
        return 'Yandex:SearchResult <{ndocs} docs (out of {expected}) from {nsites} sites>'.format(ndocs=self.ndocs_fetched(), nsites=len(self.sites_statistics.keys()), expected=self._ndocs_expected)

class YandexEngine:
    'Взаимодействие с поисковой системой.'
    def __init__(self, username, api_key):
        self.username = username
        self.api_key = api_key
        self._endpoint = 'https://yandex.com/search/xml?user={user}&key={key}&l10n=en&filter=none'.format(user=username, key=api_key)

    def query(self, query_string, site=None, max_documents=100):
        '''Выполняет запрос, заданный строкой query_string, и возвращает объект типа Result.
        
        В случае ошибки сервиса посылается исключение YandexException.
        '''
        query_result = Result()
        max_documents = max_documents or MAX_DOCUMENTS_PER_QUERY
        docs_collected = 0
        pagenumber = -1
        while docs_collected < max_documents:
            pagenumber += 1
            chunk_size = min(max_documents - docs_collected, MAX_DOCS_PER_REQUEST)
            # Выполняем запрос и сохраняем результат
            root = self._process_request(query_string, site=site, pagenumber=pagenumber)
            if root is None:
                break # Поиск не дела результатов
            docs_collected += chunk_size
            # Оценка числа документов, удовлетворяющих параметрам запроса
            for found in root.findall("./response/found"):
                try:
                    query_result._ndocs_expected = int(found.text)
                except TypeError:
                    pass
            # Описание статистики числа документов на сайте
            for groupnode in root.findall(".//grouping/group[categ]"):
                site_info = _parse_yandex_group(groupnode)
                query_result.add_siteinfo(site_info['name'], site_info['docs_count'])
            # Конкретные документы
            for docnode in root.findall(".//group/doc[@id]"):
                d = _parse_yandex_doc(docnode)
                query_result.add_doc(d)
        return query_result


    def _process_request(self, query_string, site, pagenumber=0, max_documents=None):
        """Выполняет один запрос к Яндекс.XML. 
        
        Return:
            В случае успеха возвращает корень XML-дерева.
            При пустом результате возвращает None.

        Raise:
            YandexError.
        """
        request_method = 'POST'
        endpoint = self._endpoint
        grouping_mode = 'deep' if site is None else 'flat'
        if site is not None:
            query_string = query_string + ' site:' + site
        data = self._format_request(query_string, method=request_method,
                                    grouping_mode=grouping_mode,
                                    groups_on_page=max((max_documents or MAX_DOCS_PER_REQUEST), MAX_DOCS_PER_REQUEST),
                                    pagenumber=pagenumber)
        if request_method == 'POST':
            data = data.encode('utf-8') # data should be bytes
            request = urllib.request.Request(endpoint, data)
            request.add_header('Content-Type', 'text/xml')
        elif request_method == 'GET':
            endpoint = endpoint + '&' + data
            request = urllib.request.Request(endpoint)
            data = None
        else:
            raise NotImplementedError('Unsupported HTTP request method %s.' % request_method)
        # Выполняем запрос и сохраняем результат
        with urllib.request.urlopen(request) as response:
            xml = response.read()
            root = ET.fromstring(xml)
            error_code = root.find("./response/error[@code]")
            if error_code is not None:
                code = error_code.attrib.get('code', '-1')
                if code == '15': # empty result set is not an error
                    return None
                else:
                    raise YandexException(code, error_code.text)
            return root
        return None

    def _format_request(self, query, pagenumber=0, method='POST', grouping_mode='deep', docs_in_group=3, groups_on_page=10):
        "Построение 'основной части' GET- или POST-запроса к сервису Яндекс XML."
        parameters = {
            'query': xmlescape(query) if method=='POST' else urllib.parse.quote_plus(query, safe=''),
            'pagenumber': pagenumber,
            'maxpassages': 5,
            'grouping_mode': grouping_mode, # 'deep', or 'flat',
            'grouping_attr': 'd' if grouping_mode=='deep' else '',  # empty for flat mode
            'groups_on_page': groups_on_page,
            'docs_in_group': docs_in_group,             # 1, 2, or 3
            'sortby': 'rlv',                # rlv or tm
            'sortorder': 'descending',      # ascending or descending
        }
        if grouping_mode == 'flat':
            parameters['docs_in_group'] = 1
        templates = {
            'POST': '''<?xml version="1.0" encoding="UTF-8"?>
            <request>
            <query>{query}</query>
            <groupings>
                <groupby mode="{grouping_mode}" attr="{grouping_attr}" groups-on-page="{groups_on_page}" docs-in-group="{docs_in_group}"/>
            </groupings>
            <maxpassages>
                {maxpassages}
            </maxpassages>
            <page>{pagenumber}</page>
            <sortby order="{sortorder}" priority="no">
                 {sortby}
            </sortby>
            </request>''',
            'GET': 'query={query}&sortby={sortby}.order%3D{sortorder}&groupby=attr%3D%22{grouping_attr}%22.mode%3D{grouping_mode}.groups-on-page%3D{groups_on_page}.docs-in-group%3D{docs_in_group}&maxpassages={maxpassages}&page={pagenumber}'
        }
        return templates[method].format(**parameters)

    def get_limits(self):
        """Возвращает число оставшихся запросов."""
        with urllib.request.urlopen(self._endpoint + "&action=limits-info") as response:
            xml = response.read()
            root = ET.fromstring(xml)
            first_limit = root.find("./response/limits/time-interval[1]")
            if first_limit is not None:
                limit_value = int(first_limit.text)
                return limit_value
            return None

def _parse_yandex_doc(docnode):
    "Преобразование данных о документе из XML в объект MatchingDocument."
    def tostr(n):
        return ' '.join([text.strip() for text in n.itertext()])
    d = {}
    for child in docnode:
        if child.tag in ['title', 'url', 'domain', 'charset', 'modtime']:
            d[child.tag] = tostr(child)
    d['id'] = docnode.attrib.get('id')
    d['passages'] = [tostr(passage) for passage in docnode.findall('.//passage')]
    return MatchingDocument(d.get('url'), d.get('domain'), d.get('title'), d.get('charset'), d.get('modtime'), d.get('passages'), id=d.get('id'))

def _parse_yandex_group(groupnode):
    'Если запрос использует группировку, то в группу объединяются страницы с одного сайта. Функция извлекает описание группы: имя сайта и общее число документов.'
    g = {}
    categ = groupnode.find('./categ')
    if categ is not None:
        g['name'] = categ.attrib.get('name')
    doccount = groupnode.find('./doccount')
    if doccount is not None:
        g['docs_count'] = int(doccount.text)
    return g


def _get_default_engine():
    sec = settings.config['Yandex']
    username = sec['username']
    api_key = sec['KEY']
    yandex = YandexEngine(username, api_key)
    return yandex

default_engine = _get_default_engine()
