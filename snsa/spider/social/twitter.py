from TwitterSearch import TwitterSearchOrder, TwitterSearch, TwitterSearchException
from snsa.settings import config

import time

def twitter_time_to_time(str):
    return time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(str,'%a %b %d %H:%M:%S +0000 %Y'))


def search(terms, limit=1000):
    try:
        tso = TwitterSearchOrder()  # create a TwitterSearchOrder object
        # let's define all words we would like to have a look for
        tso.set_keywords(terms)
        tso.set_language('en')  # we want to see English tweets only
        # and don't give us all those entity information
        tso.set_include_entities(False)

        # it's about time to create a TwitterSearch object with our secret tokens
        ts = TwitterSearch(
            consumer_key=config.get('Twitter', 'consumer_key'),
            consumer_secret=config.get('Twitter', 'consumer_secret'),
            access_token=config.get('Twitter', 'access_token'),
            access_token_secret=config.get('Twitter', 'access_token_secret')
        )

        # this is where the fun actually starts :)
        for index, tweet in enumerate(ts.search_tweets_iterable(tso)):
            #print('%d: (%s, %s) @%s tweeted: %s' %
            #      (index, tweet['created_at'], tweet['retweet_count'], tweet['user']['screen_name'], tweet['text']))
            if index > limit:
                break
            try:
                yield {
                    'created_at': tweet['created_at'],
                    'created_at_time': twitter_time_to_time(tweet['created_at']),
                    'retweet_count': tweet['retweet_count'],
                    'username': tweet['user']['screen_name'],
                    'text': tweet['text'],
                    'status_id': tweet['id_str'],
                    'tweet': tweet
                }
            except TwitterSearchException as e:
                print(e)

    except TwitterSearchException as e:  # take care of all those ugly errors if there are some
        print(e)
