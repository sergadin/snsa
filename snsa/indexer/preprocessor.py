# -*- coding: utf-8; -*-

from simplekv.fs import FilesystemStore
import urllib.request
from bs4 import BeautifulSoup
import html2text
import chardet
import re
import hashlib
import json
from typing import List
from snsa import settings

class FilesystemBigStore(FilesystemStore):
    '''Стандартное файловое хранилище simplekv.fs, но с созданием двух уровней директорий для балансировки числа файлов.'''
    def _build_filename(self, key):
        import os
        if len(key) <= 4:
            return os.path.abspath(os.path.join(self.root, key))
        
        subdir1 = key[0:2]
        subdir2 = key[2:4]
        filename = key[4:]
        return os.path.abspath(os.path.join(self.root, subdir1, subdir2, filename))

def _connect_document_storage():
    """Возвращает объект simplekv.Storage."""
    storage_location = settings.top_directory + '/local/ft_storage'
    return FilesystemBigStore(storage_location)


class Document(object):
    """Описание загруженного документа.

    :attribute _uri: URI загруженного документа.
    :attribute _content: содержимое документа.
    """
    store = _connect_document_storage()
    encoding = 'utf-8'

    def __init__(self, uri, autosave=False, force_reload=False):
        self.key = self.generate_key(uri)
        self._uri = uri
        self._content = None
        self._cached = (self.key in self.store)
        if (self._cached and force_reload is False):
            self.load()
        else:
            self._uri = uri
            self._content = get_text_by_url(self._uri)
            if autosave:
                self.save()

    def save(self):
        record = {
            'uri': self._uri,
            'content': self._content,
        }
        self.store.put(self.key, json.dumps(record).encode(self.encoding))
        self._cached = True

    def load(self):
        data = self.store.get(self.key)
        record = json.loads(data.decode(self.encoding))
        self._uri = record['uri']
        self._content = record['content']
        return self._content

    def content(self, start=0, end=None):
        return self._content[start:end]

    def uri(self):
        return self._uri

    def generate_key(self, uri):
        """Создание ключа документа по его адресу."""
        return hashlib.sha224(uri.encode('utf-8', errors="ignore")).hexdigest()


def _detect_charset(content):
    """Извлекает кодировку документа из HTML-тега META или угадывает ее по содержимому документа."""
    soup = BeautifulSoup(content, 'html.parser')
    for meta in soup.findAll('meta', attrs={'http-equiv': lambda x: x and x.lower() == 'content-type'}):
        charset_search = re.search(r'''charset=["']?([a-z\-0-9]*)''', meta['content'], re.IGNORECASE)
        if charset_search:
            return charset_search.group(1)
    # Угадывание кодировки по содержимому
    guessed_encoding = chardet.detect(content)
    if guessed_encoding['confidence'] > 0.5:
        return guessed_encoding['encoding']
    return None

def get_text_by_url(url, encoding=None, timeout_secs=20):
    """
        Скачивает документ и возвращает его содержимое. Перехватывает все исключения.
    """
    text = None
    try:
        print("Fetching %s" % url)
        headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'}
        req = urllib.request.Request(url, headers=headers)
        with urllib.request.urlopen(req, timeout=timeout_secs) as response:
            content = response.read()
            charset = response.headers.get_content_charset() or _detect_charset(content)
            if charset is None:
                print("No charset specified in the document; assuming utf-8.")
                charset = 'utf-8'
            content = content.decode(charset, errors="ignore")
            h = html2text.HTML2Text()
            h.ignore_links = True
            text = h.handle(content)
    except (UnicodeDecodeError, Exception) as e:
        print(e)
        return None
    return text
