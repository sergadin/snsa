﻿# -*- coding: utf-8; -*-

"""
    Классы и утилиты для упрощения работы с онтологией.
"""

# https://github.com/cosminbasca/surfrdf SuRF is a Python library for working with RDF data in an Object-Oriented manner


import rdflib
from rdflib.plugins.sparql import prepareQuery
from snsa import settings

class Ontology:
    """
    Кеширование онтологий и результатов синтаксического анализа запросов. Объекты представляют собой
    загружанную в память онтологию и предварительно разобранные запросы.
    """
    def __init__(self, filename, format="turtle"):
        """
        :param filename: имя файла. Если начинается не с символа "/", то имя считается относительно корневой директории проекта.
        """
        full_filename = filename if filename.startswith('/') else (settings.top_directory + '/' + filename) 
        self.g = rdflib.Graph()
        self.g.load(full_filename, format=format)
        self.queries = {}
        self.namespaces = dict(self.g.namespaces())

    def graph(self):
        return self.g

    def prepare(self, query, name, force_update=False):
        """Синтаксический анализ SPARQL-запроса.

        :param string query: текст SPARQL-запроса.
        :param string name: имя, которое присваивается этому запросу.
        :param boolean force_update: если истинно, то разбор запроса производится даже в том случае, когда имя name ранее использовалось.

        Подготовленные запросы сохраняются в объекте и могут в дальнейшем вычисляться без проведения разбора.
        """
        if (name not in self.queries) or force_update:
            self.queries[name] = prepareQuery(query, initNs=self.namespaces)
        return self.queries[name]

    def query(self, query_name, initBindings=None):
        """Выполнение ранее подготовленного запроса.
        
        :param query_name: имя запроса, которое использовалось при вызове :meth:`.prepare`.
        :param initBindings: словарь начальных значений параметров запроса. Передается в ``rdflib.graph.query``.
        """
        return self.g.query(self.queries[query_name], initBindings=initBindings)
