# -*- coding: utf-8; -*-

"""
Лексическая онтология частей тела и т.п.
"""

import rdflib
from rdflib.plugins.sparql import prepareQuery
from snsa import settings

from . import Ontology

HUMANS_ONTOLOGY = Ontology(filename='resources/organs.rdf', format='turtle')

def terms_for_concept(concept_name='BodyPart', convert_to_python=True):
    '''Генератор списка терминов, связанных с заданным понятием онтологии частей тела.'''
    HUMANS_ONTOLOGY.prepare(
        """SELECT DISTINCT ?organ ?term
        WHERE {
            ?organ ho:term ?term .
            ?organ a ?concept .
        }""",
        name='organ_terms')
    ho_namespace = HUMANS_ONTOLOGY.namespaces['ho']
    concept = rdflib.URIRef("%s%s" % (ho_namespace, concept_name))
    qres = HUMANS_ONTOLOGY.query('organ_terms', initBindings={'concept': concept})
    for row in qres:
        if convert_to_python:
            yield row.term.toPython()
        else:
            yield row.term
