# -*- coding: utf-8; -*-
# pylint: disable=unreachable
from sqlalchemy.sql import exists, label
from sqlalchemy import func
from snsa.utils.sqlalchemy import DBSession
from snsa.models.explorer import ForumPage
from snsa.models.tasks import ProcessingTask
from snsa.models.opinion import DocumentFragment, ExpertEvaluation
from snsa.indexer.preprocessor import Document
from snsa.nlp.tokenizer import find_paragraphs, normalize
from snsa.analyzer import analyze

class SentimentSnippet:
    def __init__(self, text, evaluation, id, expert_mark=None):
        self._text = text
        self._evaluation = evaluation
        self.id = id
        self.expert_mark = expert_mark

    @property
    def text(self):
        return self._text

    @property
    def evaluation(self):
        return self._evaluation


def page_data(forumpage_id):
    '''Загружает данные ранее проанализированной страницы. Если данные не найден в базе, то запускается анализ (это поведение следует в дальнейшем удалить).'''
    page = None
    snippets = []
    with DBSession() as session:
        query = session.query(ForumPage).filter(ForumPage.id == forumpage_id)
        page = query.first()
        snippets_query = session.query(DocumentFragment).filter(DocumentFragment.forumpage_id == forumpage_id)        
        if not session.query(snippets_query.exists()).scalar():
            analyze(forumpage_id)
        # Добавим экспертные оценки, по одной на каждый фрагмент
        # TODO: добавить фильтрацию по текущему пользователю
        marks_subquery = session.query(ExpertEvaluation.docfragment_id.label('docfragment_id'), func.max(ExpertEvaluation.evaluation).label('expert_mark'))\
            .group_by(ExpertEvaluation.docfragment_id).subquery()
        snippets_query = snippets_query.add_columns(marks_subquery.c.expert_mark).outerjoin(marks_subquery, DocumentFragment.id == marks_subquery.c.docfragment_id)

        # Загружаем содержимое документа и разбиваем его на абзацы, так как они не хранятся в базе
        doc = Document(page.url, autosave=True)
        texts = {seq: normalize(paragraph) for seq, paragraph in enumerate(find_paragraphs(doc.content()))}
        for snippet, expert_mark in snippets_query.all():
            snippets.append(SentimentSnippet(text=texts.get(snippet.seq), evaluation=snippet.evaluation, id=snippet.id, expert_mark=expert_mark))

    return snippets


def pages_for_task(taskid):
    '''Возвращает список страниц с данными о максимальной и минимальной оценкой тональности.'''
    def make_page(pg_info):
        (pg, max_eval, min_eval) = pg_info
        return {
            'domain': pg.domain, 'title': pg.title, 'id': pg.id, 'url': pg.url,
            'max_snippet_evaluation': max_eval,
            'min_snippet_evaluation': min_eval
        }
    with DBSession() as session:
        query = session.query(ProcessingTask).filter(ProcessingTask.id == taskid)
        the_task = query.first()

        # Оценка документа (максимальные по модулю значения и количество эмоционально окрашенных фрагментов)
        query = session.query(ForumPage,
                                label('max_evaluation', func.max(DocumentFragment.evaluation)),
                                label('min_evaluation', func.min(DocumentFragment.evaluation)))
        query = query.join(DocumentFragment)
        query = query.filter(ForumPage.drugname == the_task.drugname)
        query = query.group_by(ForumPage)
        pages = [make_page(x) for x in query.all()]
        return pages
