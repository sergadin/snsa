# -*- coding: utf-8; -*-
from snsa.models.tasks import ProcessingTask
from snsa.utils.sqlalchemy import default_session

def submit_task(drugname):
    session = default_session()
    similar_tasks = session.query(ProcessingTask.id).filter(ProcessingTask.drugname.ilike(drugname))
    if similar_tasks.first():
        return None
    task = ProcessingTask(drugname)
    session.add(task)
    session.commit()
    return task
