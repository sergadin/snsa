
from snsa.spider.social import twitter

def search(terms_str):
    terms = terms_str.split(',')
    return twitter.search(terms)
