'''
Обучение классификатора.
'''

import sqlalchemy
from snsa.utils.sqlalchemy import DBSession, default_session
from snsa.models.opinion import ExpertEvaluation, DocumentFragment, TrainingSet
from snsa.models.explorer import ForumPage
from snsa.indexer.preprocessor import Document
from snsa.nlp.structure import Text

from snsa.classifiers.sentiment import is_a_review

EPSILON = 0.01
def polarity_to_decision(opinion_polarity):
    if opinion_polarity is None:
        return None
    if abs(opinion_polarity) > EPSILON:
        return 'positive' if opinion_polarity > 0 else 'negative'
    return 'neutral'

def save_snippet_evaluation(snippet_id, decision, confidence=1):
    evaluation = confidence
    if decision == 'negative':
        evaluation *= -1
    with DBSession() as session:
        docfragment = session.query(DocumentFragment).get(snippet_id)
        if docfragment:
            expert_mark = ExpertEvaluation(docfragment, evaluation=evaluation)
            session.add(expert_mark)

def to_int(value):
    try:
        return int(value)
    except (ValueError, TypeError):
        return None

MAX_REVIEW_CONFIDENCE = 1

def save_training_set_mark(text, review_confidence, opinion_polarity, snippet_id=None):
    opinion_polarity = to_int(opinion_polarity)    
    session = default_session()
    if snippet_id is not None and opinion_polarity is not None:
        save_snippet_evaluation(snippet_id, polarity_to_decision(opinion_polarity), confidence=abs(opinion_polarity))
    text_hash = TrainingSet.compute_hash(text)
    old_record_q = session.query(TrainingSet).filter(TrainingSet.text_hash==text_hash)
    old_record = old_record_q.first()
    if old_record:
        if opinion_polarity is not None:
            old_record.review_confidence = (review_confidence or MAX_REVIEW_CONFIDENCE)
            old_record.opinion_polarity = opinion_polarity
        elif review_confidence is not None:
            old_record.review_confidence = review_confidence
    else:
        ts_item = TrainingSet(
            text=text,
            review_confidence=review_confidence,
            opinion_polarity=opinion_polarity,
            docfragment_id=snippet_id)
        ts_item.set_hash()
        session.add(ts_item)
    session.commit()

from sqlalchemy.orm import raiseload
def get_trining_set_items():
    with DBSession() as session:
        #--- FIXME: replace this stupid loop by an eager loading
        for item in session.query(TrainingSet):
            yield {
                'text': item.text,
                'review_confidence': item.review_confidence,
                'opinion_polarity': item.opinion_polarity,
            }


def get_snippet(snippet_id):
    session = default_session()
    docfragment = session.query(DocumentFragment).get(snippet_id)
    return docfragment


def next_snippet(user=None):
    '''Выбор очередного фрагмента для оценки пользователем.'''
    session = default_session()
    docfragments = session.query(DocumentFragment)
    docfragments = docfragments.join(ForumPage, ForumPage.id == DocumentFragment.forumpage_id)
    docfragments = docfragments.filter(
        sqlalchemy.not_(
            sqlalchemy.or_(ForumPage.title.ilike('%цена%'),
                           ForumPage.title.ilike('%описание%'),
                           ForumPage.title.ilike('%купить%'),
                           ForumPage.title.ilike('%Инструкция%'),
                           ForumPage.title.ilike('%инструкция%'))))
    docfragments = docfragments.filter(sqlalchemy.not_(DocumentFragment.expert_marks.any()),
                                       DocumentFragment.end > 1)

    # docfragment = (session.query(DocumentFragment)
    #     .filter(sqlalchemy.not_(DocumentFragment.expert_marks.any()),
    #             # DocumentFragment.forumpage.drugname == 'Мезавант',
    #             DocumentFragment.end > 1).first())

    from snsa.analyzer import evaluate_fragment

    for cnt, docfragment in enumerate(docfragments):
        doc = Document(docfragment.forumpage.url, autosave=True)
        content = doc.content(start=docfragment.start, end=docfragment.end)
        text = Text(content=content)
        if evaluate_fragment(content, docfragment=docfragment) > 0.4:
            return {
                'snippet': doc.content(start=docfragment.start, end=docfragment.end),
                'snippet_id': docfragment.id,
                'drugname': 'some drug',
                'page_title': docfragment.forumpage.title,
            }
        if cnt > 1000:
            break
    return {}


def snippets_to_evaluate(limit=50):
    session = default_session()
    docfragments = session.query(DocumentFragment)
    docfragments = docfragments.join(ForumPage, ForumPage.id == DocumentFragment.forumpage_id)
    docfragments = docfragments.filter(
        sqlalchemy.not_(
            sqlalchemy.or_(ForumPage.title.ilike('%цена%'),
                           ForumPage.title.ilike('%описание%'),
                           ForumPage.title.ilike('%купить%'),
                           ForumPage.title.ilike('%Инструкция%'),
                           ForumPage.title.ilike('%инструкция%'))))
    docfragments = docfragments.filter(sqlalchemy.not_(DocumentFragment.expert_marks.any()),
                                       DocumentFragment.end > 1)
    #docfragments = docfragments.filter(DocumentFragment.expert_marks.any())
    for index, snippet in enumerate(docfragments):
        if index >= limit:
            break
        yield snippet


from snsa.nlp.features import compute_features
def evaluate_text_for_opinion(text):
    decision, confidence = is_a_review(text)
    features = compute_features(text)
    return {
        'is_a_review': 1 if decision else 0,
        'is_a_review_confidence': float(confidence),
        'opinion_polarity': None,
        'opinion_polarity_int': None,
        'features': {
            'pers': features.get('pers', 0),
            'symptoms': features.get('symptoms', 0),
            'spoken': features.get('spoken', 0),
            'body_parts': features.get('body parts', 0),
        }
    }


