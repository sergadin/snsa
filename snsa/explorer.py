# -*- coding: utf-8; -*-

'''
    Поиск форумов с обсуждениями препарата.
'''
import logging
import re
from collections import Counter

from simhash import Simhash

from sqlalchemy import func
from sqlalchemy import not_, and_

from snsa.utils.sqlalchemy import DBSession
from snsa.models.explorer import ForumPage, SiteInfo, SiteTag, QueryTag
from snsa.models.tasks import ProcessingTask
import snsa.spider.yandex as yandex
from snsa.nlp.tokenizer import tokenize
from snsa.nlp.morph import morph
from snsa.analyzer import analyze
from snsa.indexer.preprocessor import Document


class Explorer:
    exploration_docs = 500
    """Количество документов, которые обрабатываются при первом "широком" запросе."""

    def __init__(self, drugnames, canonic_name=None):
        self.canonic_name = canonic_name or drugnames[0]
        self.drugnames = drugnames
        self.drugnames_normal_forms = [morph.parse(w)[0].normal_form for w in drugnames]

    def explore(self, number_of_top_sites=10, docs_per_site=100, search_engine :yandex.YandexEngine = yandex.default_engine):
        """Находит сайты и конкретные страницы в поисковой системе search_engine.

        Поиск выполняется в два этапа. Сначала выполняется поиск в Интернет с группировкой по сайтам. Для
        сайтов, которые содержат наибольшее число документов, выполняется уточняющий поиск.
        Собранные адреса записываются в базу данных.

        Keyword Args:
            number_of_top_sites (int): максимальное количество сайтов, для которых выполняется уточняющий запрос.
            docs_per_site (int): максимальное число документов с одного сайта.
            search_engine: поисковая система.
        """
        if _processed_p(self.canonic_name):
            return
        # Выполняем "широкий" запрос, который находит документы в Интренет с группировкой по сайтам.
        _clear_storage(self.canonic_name)
        query = _create_search_query(self.drugnames, words_to_avoid=['цена', 'инструкция', 'купить'])
        result = search_engine.query(query, max_documents=self.exploration_docs)
        collocations = _words_stat(result, exclude_nf=self.drugnames_normal_forms, scope='title')
        _store_result(result, self.canonic_name, querytags=collocations)

        print("*** Terminate exploration after first statge ***")
        print("************************************************")
        return True

        top_sites = result.sites_statistics
        exclude = _find_words_to_exclude(self.canonic_name, result, collocations)
        if exclude is not None:
            refined_query = _create_search_query(self.drugnames, words_to_avoid=exclude)
            refined_result = search_engine.query(refined_query, max_documents=self.exploration_docs)
            top_sites = top_sites + refined_result.sites_statistics

        # Для каждого сайта выполняем поиск с ограничением поиска толко по этому сайту
        for (site_name, _docs_count) in top_sites.most_common(number_of_top_sites):
            result = search_engine.query(
                _create_search_query(self.drugnames),
                site=site_name)
            _store_result(result, self.canonic_name)
        return True

    def urls(self):
        with DBSession() as session:
            for fp in session.query(ForumPage).filter(ForumPage.drugname.ilike(self.canonic_name)).all():
                yield fp.url


def _words_stat(result: yandex.Result, scope='title', exclude_nf=None, morph_score_threshold=0.3):
    """Вычисление наиболее популярных слов, встречающихся в результатах поиска. Учитываются только существительные и глаголы.
    
    Args:
        morph_score_threshold (float): минимально необходимый вес неоднозначной словоформы.

    Return:
        слова (Counter): слова с указанием числа документов, где это слово встретилось.
    """
    wordscount = Counter()
    for doc in result.documents:
        title = doc.title
        snippets = ' ' + ' '.join(doc.snippets)
        text = ((title if scope in ['title', 'all'] else '') +
                (snippets if scope in ['snippets', 'all'] else ''))
        for word in tokenize(text):
            word = word.lower()
            ps = morph.parse(word)
            if not ps or ps[0].normal_form in exclude_nf:
                continue

            def good_form(parse_form):
                return parse_form.score > morph_score_threshold and parse_form.tag.POS in ['NOUN', 'INFN', 'VERB']
            for normal_form in set([p.normal_form for p in ps if good_form(p)]):
                wordscount[normal_form] += 1
    return wordscount

def _sites_with_word(result: yandex.Result, word: str):
    sitescount = Counter()
    word_parse = morph.parse(word)
    for doc in result.documents:
        text = doc.title
        for text_word in tokenize(text):
            text_word_parse = morph.parse(text_word)
            if morph.match(word_parse, text_word_parse):
                sitescount[doc.domain] += 1
                break
    return sitescount


def _find_words_to_exclude(drugname, result :yandex.Result, collocations):
    """Поиск слов для удаления из запроса."""
    exclusions = []
    sites_affected = set()
    for frequent_word, _weight in collocations.most_common(20):
        sites = set(_sites_with_word(result, frequent_word))
        if len(sites - sites_affected) > len(sites) / 2:
            exclusions.append(frequent_word)
            sites_affected = sites_affected.union(sites)
        if len(exclusions) >= 2:
            break
    return exclusions


def popular_collocations(words, words_to_avoid=None):
    required_words = [words[0]]
    desired_words = words[1:]
    required_words_normal = [morph.parse(x)[0].normal_form for x in required_words]

    query = _create_search_query(desired_words, required_words=required_words, words_to_avoid=words_to_avoid)
    result = yandex.default_engine.query(query, max_documents=400)

    def count_words(result: yandex.Result, scope='title'):
        return _words_stat(result, scope=scope, exclude_nf=required_words_normal)
    wordscount = count_words(result)
    wordscount_snippets = count_words(result, 'snippets')
    print(query)
    print(result)
    print(wordscount.most_common(30), end='\n')
    print(wordscount_snippets.most_common(30), end='\n\n')
    
    print(result.sites_statistics.most_common(30))
    for top_site, _ in result.sites_statistics.most_common(10):
        result_for_site = yandex.default_engine.query(query, site=top_site, max_documents=100)
        words_on_site = count_words(result_for_site)
        words_on_site_snippets = count_words(result_for_site, scope='snippets')
        print(top_site + ': ', words_on_site.most_common(10))
        print(top_site + ': ', words_on_site_snippets.most_common(10))

    if words_to_avoid is None:
        to_exclude = _find_words_to_exclude(required_words[0], result, wordscount)
        return popular_collocations(words, to_exclude)
    return None


def _create_search_query(words, required_words=None, words_to_avoid=None, lang='ru'):
    def nwords(term):
        return len(term.split())
    def quote(term):
        return ('"%s"' % term if nwords(term) > 1 else term)
    desired_part = ' '.join(['{name}'.format(name=quote(name)) for name in words or []])
    required_part = ''.join([' +{name}'.format(name=quote(name)) for name in required_words or []])
    negative_part = ''.join([' -{name}'.format(name=name) for name in (words_to_avoid or []) if nwords(name) == 1])
    query = desired_part + required_part + negative_part
    if lang:
        query = query + ' lang:ru'
    return query

def _processed_p(drugname):
    'Проверяет, был ли ранее выполнен поиск для этого препарата.'
    with DBSession() as session:
        cnt = session.query(ForumPage).filter(ForumPage.drugname.ilike(drugname)).count()
        if cnt > 3:
            return True
    return False

def _clear_storage(drugname):
    with DBSession() as session:
        session.query(ForumPage).filter(ForumPage.drugname==drugname).delete()
        session.query(SiteInfo).filter(SiteInfo.drugname==drugname).delete()


def _store_result(result: yandex.Result, drugname, querytags=None):
    with DBSession() as session:
        for doc in result.documents:
            forumpage = ForumPage(drugname, url=doc.url, domain=doc.domain, title=doc.title, encoding=doc.charset, docid=doc.id)
            session.add(forumpage)
        for (site_name, docs_count) in result.sites_statistics.items():
            si = SiteInfo(drugname, site_name, docs_count)
            session.add(si)
        if querytags:
            for word, weight in querytags.items():
                querytag = QueryTag(drugname, word, 'title', weight)
                session.add(querytag)
        session.commit()


def pages_for_drugname(drugname):
    with DBSession() as session:
        for fp in session.query(ForumPage).filter(and_(ForumPage.drugname.ilike(drugname), not_(ForumPage.title.contains('цена')))).all():
            yield fp


def download_pages(taskid):
    with DBSession(commit=True) as session:
        the_task = session.query(ProcessingTask).get(taskid)
        for _seq, fp in enumerate(pages_for_drugname(the_task.drugname)):
            try:
                _doc = Document(fp.url, autosave=True)
            except Exception:
                pass

def analyze_pages(taskid):
    with DBSession(commit=True) as session:
        the_task = session.query(ProcessingTask).get(taskid)
        for _seq, fp in enumerate(pages_for_drugname(the_task.drugname)):
            try:
                analyze(fp.id)
            except Exception as e:
                print(e)

def process_all_tasks():
    return analyze_all_fragments()
    cnt = 0
    while process_tasks() and cnt < 4000:
        print("task processed; sleeping...")
        import time
        time.sleep(0.2)
        print("Ok, resumng processing")
        cnt = cnt+1

def download_all_tasks():
    with DBSession(commit=True) as session:
        query = session.query(ForumPage)
        query = query.join(ProcessingTask, ProcessingTask.drugname == ForumPage.drugname)
        query = query.filter(ProcessingTask.status == u'Explored')
        query = query.order_by(ProcessingTask.id.desc())
        for i, forum_page in enumerate(query):
            try:
                _doc = Document(forum_page.url, autosave=True)
            except Exception:
                pass
            if i > 30000:
                break
    return None


def analyze_all_fragments(fragments_limit=100):
    from snsa.analyzer import analyze
    from snsa.models.opinion import DocumentFragment
    import sqlalchemy

    with DBSession(commit=True) as session:
        pages_to_analyze = session.query(ForumPage)
        pages_to_analyze = pages_to_analyze.filter(
            sqlalchemy.not_(
                sqlalchemy.or_(ForumPage.title.like('%цена%'),
                               ForumPage.title.like('%цены%'),
                               ForumPage.title.like('%писание%'),
                               ForumPage.title.like('%упить%'),
                               ForumPage.title.like('%родам%'),
                               ForumPage.title.like('%нструкция%'))))
        fragments_with_features = session.query(DocumentFragment.forumpage_id).filter(DocumentFragment.features.any())
        fragments_with_marks = session.query(DocumentFragment.forumpage_id).filter(DocumentFragment.expert_marks.any())
        pages_to_analyze = pages_to_analyze.filter(
            sqlalchemy.not_(ForumPage.id.in_(fragments_with_features)),
            sqlalchemy.not_(ForumPage.id.in_(fragments_with_marks)))

        print(pages_to_analyze)
        for count, forumpage in enumerate(pages_to_analyze):
            print(count, forumpage.title)
            analyze(forumpage.id)
            print("*** PAGE PROCESSED ***")
            if count > 1000:
                break

        # docfragments = session.query(DocumentFragment)
        # docfragments = docfragments.join(ForumPage, ForumPage.id == DocumentFragment.forumpage_id)
        # docfragments = docfragments.filter(
        #     sqlalchemy.not_(
        #         sqlalchemy.or_(ForumPage.title.ilike('%цена%'),
        #                        ForumPage.title.ilike('%описание%'),
        #                        ForumPage.title.ilike('%купить%'),
        #                        ForumPage.title.ilike('%продам%'),
        #                        ForumPage.title.ilike('%инструкция%'))))
        # docfragments = docfragments.filter(
        #     DocumentFragment.end > 1,
        #     sqlalchemy.not_(DocumentFragment.expert_marks.any()),
        #     sqlalchemy.not_(DocumentFragment.features.any()))
        # print("***************************************")
        # for count, docfragment in enumerate(docfragments):
        #     doc = Document(docfragment.forumpage.url, autosave=True)
        #     content = doc.content(start=docfragment.start, end=docfragment.end)
        #     print(docfragment.id, docfragment.forumpage.title)
        #     #evaluate_fragment(content, docfragment=docfragment)

        #     if count > fragments_limit:
        #         break
        # print(">>> ***************************************")
    return True


def process_tasks():
    """Выбирает одно (последнее добавленное) задание на поиск и выполняет скачивание документов."""

    the_task = None
    with DBSession(commit=True) as session:
        query = session.query(ProcessingTask).filter(ProcessingTask.status == u'created').order_by(ProcessingTask.id.desc()) #--- FIXME: add special DB column for filtering
        the_task = query.first()
        if the_task:
            drugnames_str = the_task.drugname + '; ' + (the_task.aliases or '')
            drugnames = [name for name in re.split(r'\s*;\s*', drugnames_str) if name]
            logging.info('Processing new task for "%s"...' % drugnames)
            the_task.update_status('Exploring')
            session.commit()
            explorer = Explorer(drugnames)
            explorer.explore(number_of_top_sites=25)
            the_task.update_status('Explored')
            session.commit()
        else:
            return None

    the_task.update_status('Downloading')
    for _, fp in enumerate(pages_for_drugname(the_task.drugname)):
        try:
            _doc = Document(fp.url, autosave=True)
        except Exception:
            pass

    the_task.update_status('Analyzing')
    for _, fp in enumerate(pages_for_drugname(the_task.drugname)):
        analyze(fp.id)
    the_task.update_status('Ready')

    return the_task

