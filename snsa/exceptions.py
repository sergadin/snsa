# -*- coding: utf-8; -*-

"""
Исключения.
"""

class SNSAException(Exception):
    pass

class SpiderException(SNSAException):
    """Исключения подсистемы скачивания документов.
    """
    pass
