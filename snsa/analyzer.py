# -*- coding: utf-8; -*-
# pylint: disable=unreachable
from snsa.nlp.features import compute_features
from snsa.utils.sqlalchemy import DBSession
from snsa.models.explorer import ForumPage
from snsa.models.opinion import DocumentFragment, FragmentFeatures
from snsa.indexer.preprocessor import Document
from snsa.nlp.structure import Text
import numpy as np

def _is_a_review(features):
    return (features.get('pers_freq', 0) > 0.05
            or features.get('pos/neg_max', 0) >= 2
            or features.get('pos/neg', 0) >= 5
            or (features.get('pers_freq', 0) * features.get('doclen', 0) > 10))


def analyze_fragment(text :str, drugname=None, features=None):
    features = features or compute_features(text)
    _contains_drugname = text.find(drugname)
    if _is_a_review(features):
        if features.get('more/less_med', 0) > 0:
            return features.get('more/less_max', 0)
        else:
            return features.get('more/less_min', 0)
    return 0

def evaluate_fragment(text :str, drugname=None, docfragment=None):
    from snsa.models.opinion import FragmentFeatures
    from snsa.utils.sqlalchemy import default_session
    features = compute_features(text)
    if docfragment is not None and not docfragment.features:
        session = default_session()
        saved_features = FragmentFeatures(
            docfragment_id=docfragment.id,
            usage=features['usage'],
            symptoms=features['symptoms'],
            body=features.get('body parts'),
            time=features.get('time'),
            appear_disappear=features.get('app/disapp'),
            positive_negative=features.get('pos/neg'),
            more_less=features.get('more/less'),
            spoken=features.get('spoken'),
            personal=features.get('pers'),
            punctuation=features.get('punct'))
        session.add(saved_features)
    print(features)
    if features['pers_freq'] > 0.15 and features['punct'] > 0:
        return 1
    return 0


def sigmoid(x, scale=10):
    return scale/(1 + np.exp(-x)) - (scale/2) if x is not None else None

def analyze(forumpage_id):
    from snsa.utils.sqlalchemy import default_session
    page = None
    session = default_session()
    page = session.query(ForumPage).get(forumpage_id)
    session.query(DocumentFragment).filter(DocumentFragment.forumpage_id == page.id).delete()

    drugname = page.drugname
    doc = Document(page.url, autosave=True)
    print('------------- Document loaded. Content length: ', len(doc._content) if doc._content else None, '----------------------------')
    if doc._content is None or len(doc._content) > 200*1024: # 200K is too long for the implementation and/or virtual machine
        return False
    text = Text(document=doc)
    with DBSession() as conn:
        for seq, paragraph in enumerate(text.paragraphs()):
            if not paragraph or paragraph.length() < 100 or paragraph.length() > 10*1000:
                continue
            print("*** New paragraph found ***")
            paragraph_content = paragraph.text_content()
            features = compute_features(paragraph_content)
            evaluation = sigmoid(analyze_fragment(paragraph_content, drugname=drugname, features=features))
            print("*** SAVING... ***")
            # Сохраняем информацию о фрагменте
            fragment = DocumentFragment(forumpage=page, seq=seq, start=paragraph.start, end=paragraph.end, evaluation=evaluation)
            conn.add(fragment)
            conn.flush()
            # Сохраняем посчитанные лингвистические признаки фрагмента
            fragment_features = FragmentFeatures(docfragment_id=fragment.id)
            fragment_features.from_features_dict(features, docfragment=fragment)
            conn.add(fragment_features)
            print("*** DONE ***")
    return True
