﻿# -*- coding: utf-8; -*-

'''
    Вспомогательные классы и декораторы для работы с SQLAlchemy.
'''

from snsa.settings import DB

class DBSession():
    '''
        Пример:
        with DBSession() as session:
            session.query()
            do_something_more()
    '''
    def __init__(self, commit=True):
        self.commit = commit
    def __enter__(self):
        self.session = DB.Session()
        return self.session
    def __exit__(self, type, value, traceback):
        if self.commit:
            self.session.commit()

def dbconnect(func):
    '''
    Декоратор, который в случае необходимости создает новое соединение с базой и по окончании функции выполняет COMMIT/ROLLBACK.

    Пример:
        @dbconnect
        def some_function(some, arguments, session)
            session.query(...) # no commit, close, rollback required
    '''
    def inner(*args, **kwargs):
        session = DB.Session()  # with all the requirements
        try:
            func(*args, session=session, **kwargs)
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()
    return inner


def default_session():
    return DB.Session()
