# -*- coding: utf-8; -*-
"""
    Вычисление различных классификационных признаков.
"""

# from dataclasses import dataclass

from snsa.nlp.tokenizer import tokenize
from snsa.nlp.morph import morph
from snsa.ontology.human import terms_for_concept
from snsa.nlp.structure import Text, Passage, LocationInfo
import numpy

USAGE = ['введение', 'применение', 'принял', 'прием', 'укол', 'колоть', 'капать', 'вливание', 'вливать']
SYMPTOMS = ['температура', 'пульс', 'давление', 'одышка',  'головокружение', 'утомляемость',
            'озноб', 'насморк', 'ломота', 'боль', 'кашель', 'слабость', 'жжение', 'сухость', 'зуд', 'сердцебиение', 'отек',
            'тошнота', 'рвота',
            'дискомфорт', 'недомогание',
            'воспаление', 'сыпь', 'краснота', 'покраснение', 'бледность', 'побледнение', 'синюшность',
            'депрессия']
AFTER = ['после']
TIME = ['час', 'день', 'неделя', 'месяц', 'год']

APPEAR_DISAPPEAR = {
    1: ['появилось', 'началось'],
    -1: ['прошло', 'пропало', 'прекратилось']
}
POSITIVE_NEGATIVE = {
    3: ['супер', 'суперский', 'здорово', 'классно'],
    2: ['отлично'],
    1: ['хорошо', 'лучше'],
    -1: ['плохо', 'хуже'],
    -2: ['ужасно']
}
MORE_LESS = {
    1: ['сильнее', 'чаще', 'больше', 'более', 'повышаться', 'увеличиваться', 'расти', 'усиливается'],
    -1: ['слабее', 'меньше', 'менее', 'снижение', 'уменьшение', 'уменьшаться', 'реже', 'ослабевает'],
}
IMPROVERS = {
    2: ['существенно', 'гораздо', 'значительно', 'значительное'],
    1: ['несколько', 'немного', 'незначительно', 'слишком'],
    0.5: ['чуть', 'чуть-чуть'],
}

BODY_PARTS = list(terms_for_concept(concept_name='BodyPart'))


def count_personal_grammems(passage :Passage):
    """Вычисляет количество вхождений слов с определенными морфологическими характеристиками."""
    what_to_count = [
        ('npro',        {'NPRO', '1per'}), # местоимение-существительное
        ('Apro',        {'ADJF', 'Apro'}), # мой
        ('past_action', {'VERB', 'past', 'indc'}), # личные глаголы прошедшее время: выпил, написал
        ('impr',        {'VERB', 'impr', 'excl'}), # повелительное наклонение, говорящий не включён в действие (выпей)
        ('spoken',      {'PRCL', 'INTJ'}), # Признаки разговорной речи: частицы (бы, же, лишь), междометия (ой)
    ]
    counts = {label: 0 for label, _ in what_to_count }
    for token in passage.tokens():
        token = token[0] if token else None
        if token is None:
            continue
        for (label, grammems) in what_to_count:
            if grammems in token.tag:
                counts[label] = counts[label] + 1
    return counts

def _punctuation(passage):
    """Подсчитывает количество 'эмоциональных' знаков пунктуации."""
    count = 0
    for token in passage.tokens():
        token = token[0] if token else None
        if token and 'PNCT' in token.tag:
            number_of_marks = token.word.count('!') + token.word.count('?')
            if number_of_marks > 1:
                count += number_of_marks
    return {'punct': count}

import re
from functools import reduce
def _match_pattern(matches, pattern):
    'Проверка наличия заданной последовательности вхождений элементов лексикона.'
    # Присвоим каждому названию лексикона букву алфавита и кодируем все вхождения строкой в этом алфавите
    codes = {chr(ord('a') + seq) : name for seq, name in enumerate(matches.keys())}
    name_to_code = {v: k for k, v in codes.items()}
    locations = []
    for name, ms in matches.items():
        locations.extend([(name_to_code[name], m.position) for m in ms])
    locations.sort(key=lambda x: x[1])
    code_word = ''.join([x[0] for x in locations])
    # Проверим наличие требуемого шаблона поиском регулярного выражения в кодовом слове.
    def append_item(regex, item):
        item_code = name_to_code.get(item, item)
        if item == '?' or regex == '':
            return regex + item_code
        return regex + '.*' + item_code
    regex = reduce(append_item, pattern, '')
    return (re.search(regex, code_word) is not None)


from snsa.nlp.lexicon import Lexicon, EvaluationLexicon, LexiconLocator, TrivialLocator
def _trivial_consequences(passage :Passage, drugname_locator):
    after = LexiconLocator(Lexicon('after', AFTER))
    appear = LexiconLocator(EvaluationLexicon('appear', APPEAR_DISAPPEAR))
    symptoms = LexiconLocator(Lexicon('symptoms', SYMPTOMS))
    usage = LexiconLocator(Lexicon('usage', USAGE))

    matches = {loc.lexicon().name() : loc.locate(passage) for loc in [after, appear, symptoms, usage]}
    return _match_pattern(matches, ['after', 'usage', '?', 'symptoms'])


def compute_features(text, drugname=None):
    '''Для заданного текста вычисляет значения классификационных признаков.'''
    passage = Passage(text, LocationInfo(Text(content=text), start=0, end=len(text)))
    imp_lex = EvaluationLexicon('improvers', IMPROVERS)
    more_less_lex = EvaluationLexicon('more/less', MORE_LESS, improvers=imp_lex)

    classes = {
        'usage': LexiconLocator(Lexicon('usage', USAGE)),
        'symptoms': LexiconLocator(Lexicon('sym', SYMPTOMS)),
        'body parts': LexiconLocator(Lexicon('body', BODY_PARTS)),
        'time': LexiconLocator(Lexicon('time', TIME)),
        # с числовой оценкой
        'app/disapp': LexiconLocator(EvaluationLexicon('pos/neg', APPEAR_DISAPPEAR)),
        'pos/neg': LexiconLocator(EvaluationLexicon('pos/neg', POSITIVE_NEGATIVE)),
        'more/less': LexiconLocator(more_less_lex)
    }
    features = {name: len(loc.locate(passage)) for name, loc in classes.items()}
    features.update(count_personal_grammems(passage))
    features.update(_punctuation(passage))

    features.update({'doclen': len(passage.tokens())})
    npers = sum(features.get(k, 0) for k in ['Apro', 'npro'])
    features.update({
        'pers': npers,
        'pers_freq': npers / (1 + features.get('doclen', 100000)) # деление на ноль и бесконечно длинный документ
        })
    features.update(
        {
            'spoken': features.get('spoken', 0),
            'spoken_freq': features.get('spoken', 0) / (1 + features.get('doclen', 100000))
        })
    # максимальные, минимальные и медианные значения оценочных лексиконнов
    def agg_matches(agg_fun, suffix, keys):
        return { k+suffix: agg_fun([0] + list(map(lambda match: match.evaluation, classes[k].locate(passage)))) for k in keys}
    for (fun, suffix) in ((max, '_max'), (min, '_min'), (numpy.median, '_med')):
        features.update(agg_matches(fun, suffix, ['pos/neg', 'more/less']))

    features['bodyparts'] = features.get('body parts')

    #_trivial_consequences(passage, None)
    return features
