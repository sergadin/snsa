# -*- coding: utf-8; -*-

"""
    Тривиальное разрешение анафорических местоимений.
"""

class AnaphoraResolver(object):
    def resolve(self, passage, index):
        """
            Разрешение анафоры для местоимения, находящегося в позиции index текста passage.

            Returns:
                None, если не удалось установить ссылку.
        """
        return None
