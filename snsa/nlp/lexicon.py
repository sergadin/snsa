# -*- coding: utf-8; -*-

"""
    Лексикон определяет значения слов и словосочетаний, их лексическую категоризацию (часть речи, морфологические свойства), взаимосвязи и т.п.

    word - слово
    phrase - словосочетание (одно или более слов)
    term - элемент лексикона
    passage - произвольный фрагмент текста
"""

from typing import List
from .structure import LocationInfo
from snsa.nlp.morph import morph
from collections import namedtuple
from snsa.nlp.structure import Passage, LocationInfo

def find_form(parse_forms, pos=None, grammems=None):
    """Выбор из доступных вариантов анализа заданной части речи и дополнительных грамматических характеристик."""
    for form in parse_forms:
        if (pos is None or form.tag.POS == pos) and (grammems is None or grammems in form.tag):
            return form
    return None

def find_main_word(phrase):
    """Нахождение главного слова именной группы."""
    parse_variants = [morph.parse(word) for word in phrase.split()]
    NounValue = namedtuple('NounValue', 'score, parse_form')
    def could_be_a_noun(parses):
        top_score = parses[0].score
        for parse in parses:
            if parse.tag.POS == 'NOUN':
                return NounValue(parse.score / top_score, parse)
        return None
    NounOccurrence = namedtuple('NounOccurrence', 'index, nounness')
    nouns_occurrences = [NounOccurrence(index, could_be_a_noun(ps)) for index, ps in enumerate(parse_variants) if could_be_a_noun(ps) is not None]
    nouns_occurrences.sort(key=lambda occ: occ.nounness.score, reverse=True)
    def try_main(main_index, form):
        gnc = {form.tag.gender, form.tag.number, form.tag.case}
        for index, ps in enumerate(parse_variants):
            if index != main_index and find_form(ps, 'ADJF', gnc) is None:
                return False
        return True
    for occurrence in nouns_occurrences:
        if try_main(occurrence.index, occurrence.nounness.parse_form):
            return (occurrence.index, occurrence.nounness.parse_form)
    return (nouns_occurrences[0].index, nouns_occurrences[0].nounness.parse_form)


class Term(object):
    """Понятие, элемент лексикона. Может быть словом или словосочетанием."""
    def __init__(self, phrase, hint_grammems=None):
        self._phrase = phrase
        self._words = phrase.split()
        self._main_word = self._words[0]
        self._morphs = [morph.parse(w, hint_grammems=hint_grammems) for w in self._words]
        self._evaluation = 0

    def morphs(self):
        return self._morphs

    def __str__(self):
        return "<Term: '%s'>" % self._phrase


class EvaluationTerm(Term):
    """Слово с оценкой."""
    def __init__(self, term, evaluation, **kwargs):
        super().__init__(term, **kwargs)
        self._evaluation = evaluation

    def __str__(self):
        return "<Term: '%s', value=%s>" % (self._phrase, self._evaluation)

LEXICONS_REGISTRY = {}

class Lexicon(object):
    def __init__(self, name, phrases :List[str] = []):
        self._name = name # название
        self._terms = []
        self._improvers = None
        for phrase in phrases:
            self.add_term(Term(phrase))
        LEXICONS_REGISTRY[name] = self
    
    @classmethod
    def find_lexicon(name :str):
        return LEXICONS_REGISTRY.get(name)

    def add_term(self, term):
        self._terms.append(term)

    def find(self, token):
        for lex_term in self._terms:
            if morph.match(lex_term.morphs()[0], token):
                return lex_term
        return None

    def __contains__(self, key):
        return self.find(key) is not None

    def name(self):
        return self._name

class EvaluationLexicon(Lexicon):
    def __init__(self, name, terms_by_values, improvers=None):
        super().__init__(name)
        self._improvers = improvers
        for (value, terms) in terms_by_values.items():
            for term in terms:
                self.add_term(EvaluationTerm(term, value, hint_grammems=['NOUN,inan,nomn', 'VERB', 'INFN']))

class Match(LocationInfo):
    '''
        Информация о вхождении элемента лексикона во фрагмент текста. Записывается:

        * position - позиция
        * term - найденное понятие лексикона
        * evaluation :int - оценка тональности
        * negation_match, emphasis_match - ссылки на найденные вхождения отрицания и усиливающих местоимений
    '''
    def __init__(self, position :int, term :Term, evaluation=None):
        self.position = position
        self.evaluation = evaluation or term._evaluation
        self.term = term
        self.negation_match = None
        self.emphasis_match = None

    def __str__(self):
        if self.evaluation == 0:
            value = ''
        else:
            value = '(' + ('-' if self.evaluation < 0 else '+') + str(self.evaluation) + ')'
        return 'Match {term} {eval} at {pos}'.format(pos=self.position, term=self.term, eval=value)


class LexiconLocator(object):
    '''
        Поиск вхождений слов указанного лексокона в текст.
    '''
    def __init__(self, lexicon: Lexicon):
        assert lexicon is not None, "Unable to create LexiconLocator for empty lexicon object."
        self._lexicon = lexicon

    def lexicon(self):
        return self._lexicon

    # TODO: добавить ограничение по грамматическим характеристикам
    def locate(self, passage :Passage, start=0, end=None):
        """
        Поиск вхождений слов лексикона во фрагмент ''passage''.

        Args:
            passage (Passage): фрагмент текста для анализа.
            start (int): первый токен.
            end (int): последний токен (исключен из обработки).

        Return:
            Список объектов типа Match.
        """
        window_size = 3 # поиск отрицаний и усилений
        matches = []
        for seq, p_token in enumerate(passage.tokens(start=start, end=end)):
            seq = seq + start
            term = self._lexicon.find(p_token)
            if term is not None:
                match = Match(seq, term)
                # наличие отрицания или усиления
                if isinstance(self._lexicon, EvaluationLexicon) and self._lexicon._improvers is not None:
                    match.evaluation = term._evaluation
                    emphasis_locator = LexiconLocator(self._lexicon._improvers)
                    emphs_at = emphasis_locator.locate(passage, start=max(seq-window_size, 0), end=seq)
                    if emphs_at:
                        match.emphasis_match = emphs_at[-1]
                        match.evaluation += match.emphasis_match.evaluation
                    for back_pos in range(seq-1, max(seq-window_size, 0), -1):
                        if morph.is_negation(passage[back_pos][0]):
                            match.negation_match = Match(back_pos, term=None, evaluation=1)
                            match.evaluation *= -1
                matches.append(match)
        return matches


class TrivialLocator(LexiconLocator):
    'Поиск одного из заданных слов.'
    def __init__(self, words):
        lex = Lexicon('trivial/%s' % str(id(self)), words)
        super().__init__(lex)
