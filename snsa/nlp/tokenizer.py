# -*- coding: utf-8; -*-

"""Различные функции разделения текстовых данных: токенизации строки, разделения документа на абзацы и т.п.
"""

import re
from snsa import settings


PUNCTUATION = r'([().,?!:;]+)'

def tokenize(sentence):
    """Разделяет строку на токены.

    Args:
        sentence(str): сторока для анализа.

    Returns:
        list: список строк.
    """
    spaced_sentence = re.sub(PUNCTUATION, r' \1 ', sentence)
    return spaced_sentence.split()

def find_paragraphs(text):
    """Разделяет текст на абзацы.

    Args:
        text (str): текст для анализа.

    Returns:
        list: Список абзацев.
        list: Список позиций абзацев в тексте. Каждая позиция представляет собой пару смещений [s, e],
              такую, что text[s:e] является текстом абзаца.
    """
    if not text:
        return []
    sections = []
    begin, end = 0, len(text)
    for m in re.finditer(r'\n([ \t]*\n)+', text):
        if begin < m.start():
            sections.append([begin, m.start()])
        begin = m.end()
    if begin < end:
        sections.append([begin, end])
    return [(text[b:e], b, e) for [b,e] in sections]


def normalize(string):
    'Удаляет многократные пробелы, концы строк и т.п.'
    s = re.sub(r'[ \n\t]+', ' ', string)
    s = re.sub(r'^[ \t\n]', '', s)
    s = re.sub(r'[ \t\n]$', '', s)
    return s
