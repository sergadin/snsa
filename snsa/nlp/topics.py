"""
    Тематическое моделирование коллекции документов.
"""

# TODO: собрать "большой" словарь. Документы результатов широкого запроса построить тематическую модель.

import gensim
from gensim import corpora
from collections import defaultdict
from snsa.explorer import Explorer
from snsa.nlp.structure import Text
from snsa.indexer.preprocessor import Document
from snsa.nlp.morph import morph

class DrugCorpusGenerator(object):
    """
        Генератор корпуса документов, соответствующих заданному препарату.
    """
    def __init__(self, drugname, dictionary):
        self.drugname = drugname
        self.explorer =  Explorer([drugname])
        self.dictionary = dictionary

    def __iter__(self):
        for _seq, url in enumerate(self.explorer.urls()):
            if _seq < 30:
                continue
            doc = Document(url, autosave=True)
            text = Text(document=doc)
            for passage in text.paragraphs():
                yield self.dictionary.doc2bow([toks[0].normal_form for toks in passage.tokens()])
            if _seq > 50:
                break

def build_dictionary(drugname :str):
    explorer =  Explorer([drugname])
    dictionary = corpora.Dictionary()
    for _seq, url in enumerate(explorer.urls()):
        doc = Document(url, autosave=True)
        text = Text(document=doc)
        def valuable_words(p):
            return [word for word in p.content() if not morph.is_stopword(word)]
        dictionary.add_documents([valuable_words(p) for p in text.paragraphs()])
        if _seq > 100:
            break
    dictionary.filter_extremes(keep_n=5000)
    return dictionary


def build_topic_model(drugname :str):
    dictionary = build_dictionary(drugname)
    dc = DrugCorpusGenerator(drugname, dictionary)
    lda = gensim.models.ldamodel.LdaModel(corpus=dc, id2word=dictionary, num_topics=50, update_every=1, chunksize=10000, passes=1)
    lda.print_topics(10)
    return lda
