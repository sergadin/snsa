# -*- coding: utf-8; -*-
"""
    Морфологический анализ.
"""

import pymorphy2
from pymystem3 import Mystem

# Инициализация морфологических анализаторов.
pymorphy_morph = pymorphy2.MorphAnalyzer()
mystem = Mystem()


class Morph:
    """
        Морфологический анализатор.
    """
    def parse(self, word :str, hint_grammems=None):
        """Морфологический анализ слова.

        В дальнейшем эта функция может выполнять специальную обработку нестандартных слов, например, названий препаратов.
        Или совместно с pymorphy использовать Mystem.

        Args:

            word (str): Анализируемое слово.
        
            hint_grammems: Граммемы предпочтительного разбора.
            
                Значение может быть строкой, множеством или списком строк/множеств. Если значение типа str, то граммемы
                должны быть перечислены через запятую. Например, 'NOUN, nomn' - существительное в именительном падеже.
                Если множество, то это множество строк, каждая из которых является граммемой: {'Noun', 'nomn'}. Список
                означает, что предпочтительным варианта анализа является такой, который удовлетворяет какому-либо элементу
                списка.
        
        Returns:
            List[pymorphy.Parse]: Список результатов разбора.
        """
        all_forms = pymorphy_morph.parse(word)
        selected_forms = []
        if hint_grammems:
            # Преобразуем к списку множеств
            def to_set(grammems):
                return grammems if isinstance(grammems, set) else set([g.strip() for g in grammems.split(',')])
            hint_grammems = [to_set(g) for g in ([hint_grammems] if isinstance(hint_grammems, (str, set)) else hint_grammems)]

            def match_grammems(form, grammems):
               return (set([g.strip() for g in grammems.split(',')]) if isinstance(grammems, str) else grammems) in form.tag
            for hint in hint_grammems:
                for form in all_forms:
                    if match_grammems(form, hint):
                            selected_forms.append(form)
        # Если подсказки не привели к успеху, то возвращаем все
        if not selected_forms:
            selected_forms = all_forms
        return selected_forms

    def match(self, parse1, parse2):
        """Проверяет, что среди результатов (неоднозначных) разборов двух слов есть совпадающие."""
        for p1 in parse1:
            for p2 in parse2:
                try:
                    if p1.normal_form == p2.normal_form and p1.tag.POS == p2.tag.POS:
                        return True
                except AttributeError as e:
                    print(e)
                    pass
        return False

    def is_stopword(self, word, grammems=['CONJ', 'PREP'], extra_tags=['PNCT', 'NUMB']):
        try:
            best_choice = self.parse(word)[0]
            return (best_choice.tag.POS in grammems
                    or any(filter(lambda tag: tag in best_choice.tag, extra_tags)))
        except (AttributeError, IndexError):
            pass
        return False

    def is_negation(self, token):
        return token.word.lower() == 'не'

morph = Morph()
