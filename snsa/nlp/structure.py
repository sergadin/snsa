# -*- coding: utf-8; -*-

"""
    Классы для работы с текстом и его частями.
"""
from typing import List

from snsa.nlp.tokenizer import tokenize, find_paragraphs
from snsa.nlp.morph import morph


class LocationInfo(object):
    '''Информация о позиции объекта в линейном представлении текста.'''
    def __init__(self, text, start: int, end: int):
        self.start = start
        self.end = end
        self._text = text

    def length(self):
        return self.end - self.start + 1

    def text(self):
        return self._text


class Text(object):
    '''Текст на естественном языке.'''
    def __init__(self, document=None, content=None):
        self.document = document
        self.content = content or document.content()
        self.end = len(self.content)
        self._paragraphs = []
        self._find_paragraphs()

    def paragraphs(self) -> List['Paragraph']:
        return self._paragraphs

    def _find_paragraphs(self):
        for (paragraph_content, begin, end) in find_paragraphs(self.content):
            self._paragraphs.append(Paragraph(paragraph_content, LocationInfo(self, begin, end)))


class LinearStructureItem(LocationInfo):
    def __init__(self, location: LocationInfo):
        LocationInfo.__init__(self, location.text, location.start, location.end)


class LinearStructure(object):
    def __init__(self, text: Text):
        self._text = text
    pass


class Passage(LinearStructureItem):
    """
        Фрагмент текста, например, абзац.
    """
    def __init__(self, content: str, location: LocationInfo):
        LinearStructureItem.__init__(self, location)
        self._tokens = [morph.parse(word) for word in tokenize(content)]
        self._content = content

    def tokens(self, start=0, end=None):
        'Возвращает список токенов фрагмента.'
        return self._tokens[start:end]

    def content(self):
        for toks in self._tokens:
            yield toks[0].normal_form

    def text_content(self) -> str:
        '''Текст фрагмента.'''
        return self._content

    def recover_text(self, start=0, end=None):
        return ' '.join([token[0].word for token in self._tokens[start:end]])

    def __getitem__(self, key):
        return self._tokens[key]


class Paragraph(Passage):
    '''Абзац.'''

    class SentenceLocation(LocationInfo):
        '''Информация о позиции предложения.'''
        pass

    def __init__(self, content: str, location: LocationInfo):
        super().__init__(content, location)
        self._sentences = []
        self.split_sentences()

    def split_sentences(self):
        current_sentence_start = 0
        for (index, tok) in enumerate(self._tokens):
            token = tok[0] if tok else None
            if token and 'PNCT' in token.tag:
                if token.word == '.' or token.word.startswith('?') or token.word.startswith('!'):
                    self._sentences.append(self.SentenceLocation(self.text, current_sentence_start, index))
                    current_sentence_start = index + 1
