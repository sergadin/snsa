# -*- coding: utf-8; -*-

"""
    Модели, связанные с выполнением заданий.
"""
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy import ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func
from sqlalchemy.orm.session import Session

from snsa.settings import DB

class ProcessingTask(DB.Base):
    """
        Задание на анализ препарата.
    """
    __tablename__ = 'proctask'

    id = Column('f_proctask_id', Integer, primary_key=True)
    drugname = Column('f_proctask_drugname', String, info={'notes': 'Название препарата'})
    aliases = Column('f_proctask_aliases', String, info={'notes': 'Другие названия'})
    status = Column('f_proctask_status', String)
    date = Column('f_proctask_date', DateTime, default=func.now(), onupdate=func.now())

    def __repr__(self):
        return "<Task(drugname='%s', status ='%s')>" % (
            self.drugname, self.status)

    def __init__(self, drugname):
        self.drugname = drugname
        self.status = 'created'

    def update_status(self, status):
        if status == self.status:
            return
        session = Session.object_session(self)
        logitem = TaskLog(self.id, status=self.status, date=self.date)
        session.add(logitem)
        self.status = status

class TaskLog(DB.Base):
    __tablename__ = 'tasklog'

    id = Column('f_tasklog_id', Integer, primary_key=True)
    status = Column('f_tasklog_status', String)
    date = Column('f_tasklog_date', DateTime)
    processing_task_id = Column('f_proctask_id', Integer, ForeignKey("proctask.f_proctask_id"))

    def __repr__(self):
        return "<TaskLog(prctask='%s', status ='%s')>" % (
            self.processing_task_id, self.status)

    def __init__(self, processing_task_id, status, date):
        self.processing_task_id = processing_task_id
        self.status = status
        self.date = date
