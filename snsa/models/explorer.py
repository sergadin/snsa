"""
    Модели, связанные с поиском новых ресурсов для индексирования.
"""

from sqlalchemy import Column, Integer, String, Numeric
from sqlalchemy import ForeignKey
from sqlalchemy.ext.declarative import declarative_base

from snsa.settings import DB


class ForumPage(DB.Base):
    """
        Результаты поиска в Яндекс страниц с обсуждениями препаратов.
    """
    __tablename__ = 'forumpage'

    id = Column('f_forumpage_id', Integer, primary_key=True)
    drugname = Column('f_forumpage_drugname', String,
                      info={'notes': 'Название препарата'})
    url = Column('f_forumpage_url', String)
    domain = Column('f_forumpage_domain', String)
    title = Column('f_forumpage_title', String)
    encoding = Column('f_forumpage_encoding', String)
    docid = Column('f_forumpage_docid', String, info={
                   'notes', 'Номер документа в поисковой системе'})

    def __repr__(self):
        return "<ForumPage(drugname='%s', domain ='%s', title='%s')>" % (
            self.drugname, self.domain, self.title)

    def __init__(self, drugname, url, domain, title, encoding, docid=None):
        self.drugname = drugname
        self.url = url
        self.title = title
        self.domain = domain
        self.encoding = encoding
        self.docid = docid


class SiteInfo(DB.Base):
    """
        Статистика количества удовлетворящих запросу документов на сайте.
    """
    __tablename__ = 'siteinfo'

    id = Column('f_siteinfo_id', Integer, primary_key=True)
    drugname = Column('f_siteinfo_drugname', String, info={
                      'notes': 'Название препарата (поисковый запрос)'})
    domain = Column('f_siteinfo_domain', String)
    docscount = Column('f_siteinfo_docscount', Integer)

    def __repr__(self):
        return "<SiteInfo(drugname='%s', domain ='%s', count='%d')>" % (
            self.drugname, self.domain, self.count)

    def __init__(self, drugname, domain, count):
        self.drugname = drugname
        self.domain = domain
        self.docscount = count


class QueryTag(DB.Base):
    """Наиболее популярные слова документов, найденных при поиске препарата.
    """
    __tablename__ = 'querytag'

    id = Column('f_querytag_id', Integer, primary_key=True)
    drugname = Column('f_querytag_drugname', String,
                      info={'notes': 'Название препарата'})
    tag = Column('f_querytag_tag', String,
                 info={'notes': 'Нормальная форма слова или словосочетания'})
    category = Column('f_querytag_category', String,
                      info={'notes': 'Тип слова: заголовок, контекст, текст и т.п.'})
    weight = Column('f_querytag_weight', Numeric,
                    info={'notes': 'Вес слова из [0, 1]'})

    def __init__(self, drugname, tag, category, weight):
        self.drugname = drugname
        self.tag = tag
        self.category = category
        self.weight = weight


class SiteTag(DB.Base):
    """Наиболее популярные слова документов сайта в контексте запроса.
    """
    __tablename__ = 'sitetag'

    id = Column('f_sitetag_id', Integer, primary_key=True)
    siteinfo_id = Column('f_siteinfo_id', Integer,
                         ForeignKey("siteinfo.f_siteinfo_id"))
    tag = Column('f_sitetag_tag', String,
                 info={'notes': 'Нормальная форма слова или словосочетания'})
    category = Column('f_sitetag_category', String,
                      info={'notes': 'Тип слова: заголовок, контекст, текст и т.п.'})
    weight = Column('f_sitetag_weight', Numeric,
                    info={'notes': 'Вес слова из [0, 1]'})

    def __init__(self, siteinfo, tag, category, weight):
        self.siteinfo_id = siteinfo.id
        self.tag = tag
        self.category = category
        self.weight = weight
