# -*- coding: utf-8; -*-

"""
    Модели, связанные с оценкой тональности фрагментов найденных документов.
"""
from sqlalchemy import Column, Integer, String, DateTime, Numeric, Boolean
from sqlalchemy import ForeignKey, UniqueConstraint, Index
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship

from snsa.settings import DB

class DocumentFragment(DB.Base):
    """
        Описание фрагмента документа (позиция в тексте, оценка и т.п.).
    """
    __tablename__ = 'docfragment'

    id = Column('f_docfragment_id', Integer, primary_key=True)
    seq = Column('f_docfragment_seq', Integer, info={'notes': 'Порядковый номер фрагмента.'})
    forumpage_id = Column('f_forumpage_id', Integer, ForeignKey("forumpage.f_forumpage_id"))
    start = Column('f_docfragment_start', Integer, info={'notes': 'Начало фрагмента в линейной структуре документа.'})
    end = Column('f_docfragment_end', Integer, info={'notes': 'Конец фрагмента в линейной структуре документа.'})
    evaluation = Column('f_docfragment_value', Numeric, info={'notes': 'Оценка тональности [-10, 10]'})

    expert_marks = relationship("ExpertEvaluation", back_populates="docfragment")
    forumpage = relationship('ForumPage', backref='fragments', load_on_pending=True)
    features = relationship("FragmentFeatures", back_populates="docfragment")

    def __repr__(self):
        return "<DocFragment(pageid='%s', [%s-%s])>" % (
            self.forumpage_id, self.start, self.end)

    def __init__(self, forumpage, seq, start, end, evaluation):
        self.forumpage_id = forumpage.id
        self.seq = seq
        self.start = start
        self.end = end
        self.evaluation = evaluation

    def content(self):
        from snsa.indexer.preprocessor import Document
        doc = Document(self.forumpage.url, autosave=True)
        content = doc.content(start=self.start, end=self.end)
        return content


class ExpertEvaluation(DB.Base):
    """
    Экспертная оценка тональности фрагмента.
    """
    __tablename__ = 'dfexpeval'

    id = Column('f_dfexpeval_id', Integer, primary_key=True)
    docfragment_id = Column('f_docfragment_id', Integer, ForeignKey('docfragment.f_docfragment_id', onupdate="CASCADE", ondelete="SET NULL"), nullable=False)
    user = Column('f_user_id', Integer, info={'notes': 'Пользователь, установивший экспертную оценку'})
    irrelevant = Column('f_dfexpeval_irrelevant', Boolean, info={'notes': 'Отметка о нерелевантности фрагмента (например, фрагмент не является отзывом).'})
    evaluation = Column('f_dfexpeval_value', Numeric, info={'notes': 'Экспертная оценка тональности [-10, 10].'})
    date = Column('f_dfexpeval_date', DateTime, default=func.now(), onupdate=func.now())

    docfragment = relationship('DocumentFragment', back_populates='expert_marks', load_on_pending=True)
    UniqueConstraint('docfragment_id', 'user', name='uix_expev_usermark')

    def __init__(self, docfragment, evaluation=None, irrelevant=False):
        self.docfragment_id = docfragment.id
        self.irrelevant = irrelevant
        self.evaluation = evaluation
    

class FragmentFeatures(DB.Base):
    """
    Вычисленные лингвистические признаки для фрагмента.
    """
    __tablename__ = 'dffeatures'

    id = Column('f_dffeatures_id', Integer, primary_key=True)
    docfragment_id = Column('f_docfragment_id', Integer, ForeignKey('docfragment.f_docfragment_id', onupdate="CASCADE", ondelete="SET NULL"), nullable=False)
    usage = Column('f_dffeatures_usage', Numeric)
    symptoms = Column('f_dffeatures_symptoms', Numeric)
    body = Column('f_dffeatures_body', Numeric)
    time = Column('f_dffeatures_time', Numeric)
    appear_disappear = Column('f_dffeatures_appear', Numeric)
    positive_negative = Column('f_dffeatures_posneg', Numeric)
    more_less = Column('f_dffeatures_moreless', Numeric)
    spoken = Column('f_dffeatures_spoken', Numeric)
    personal = Column('f_dffeatures_personal', Numeric)
    punctuation = Column('f_dffeatures_punct', Numeric)

    docfragment = relationship('DocumentFragment', back_populates='features', load_on_pending=True)

    Index('i_ff_dfrag_id', 'docfragment_id')


    def from_features_dict(self, features, docfragment=None):
        "Изменяет данные в соответствии с переданным словарем значений признаков."
        if docfragment is not None:
            if isinstance(docfragment, int):
                self.docfragment_id = docfragment
            else:
                self.docfragment_id = docfragment.id
        self.usage = features['usage']
        self.symptoms = features['symptoms']
        self.body = features.get('body parts')
        self.time = features.get('time')
        self.appear_disappear = features.get('app/disapp')
        self.positive_negative = features.get('pos/neg')
        self.more_less = features.get('more/less')
        self.spoken = features.get('spoken')
        self.personal = features.get('pers')
        self.punctuation = features.get('punct')
        return self


class TrainingSet(DB.Base):
    """
    Вычисленные лингвистические признаки для фрагмента.
    """
    __tablename__ = 'training'

    id = Column('f_training_id', Integer, primary_key=True)
    text = Column('f_training_text', String)
    text_hash = Column('f_training_texthash', String)
    docfragment_id = Column('f_docfragment_id', Integer, ForeignKey('docfragment.f_docfragment_id', onupdate="CASCADE", ondelete="SET NULL"), nullable=True)
    review_confidence = Column('f_training_review', Numeric)
    opinion_polarity = Column('f_training_polarity', Numeric)

    Index('i_tset_dfrag_id', 'docfragment_id')
    Index('i_tset_text_hash', 'text_hash')

    @staticmethod
    def compute_hash(text):
        import hashlib
        return hashlib.sha224(text.lower().encode('utf-8')).hexdigest()

    def set_hash(self):
        self.text_hash = TrainingSet.compute_hash(self.text)
