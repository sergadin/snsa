# -*- coding: utf-8; -*-

import logging
import configparser
from os.path import join, dirname

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

class DBInfo:
    Base = declarative_base()
    def __init__(self):
        self.engine = None
        self.Session = None

DB = DBInfo()
config = None
top_directory = '.'

def init():
    global config
    global top_directory

    logging.basicConfig(level=logging.DEBUG)

    print("Initializing SNSA...")
    top_directory = dirname(dirname(__file__)) + '/'

    config = configparser.ConfigParser()
    config_filename = join(dirname(__file__), '../local.cfg')
    config.read(config_filename)
    init_sqlalchemy()

def init_sqlalchemy():
    global DB

    sqlite_filename = join(dirname(__file__), '../local/yandex.sqlite')
    DB.engine = create_engine(r'sqlite:///{filename}'.format(filename=sqlite_filename), echo=True)
    session_factory = sessionmaker(bind=DB.engine)
    DB.Session = scoped_session(session_factory)
