from pprint import pprint
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import sqlalchemy

import snsa.settings as settings
from snsa.models.opinion import ExpertEvaluation
from snsa.utils.sqlalchemy import default_session

from snsa.nlp.features import compute_features
from snsa.indexer.preprocessor import Document

def features_to_np(features):
    '''Возвращает массив значений всех признаков в алфавитном порядке их названий.'''
    pprint(features)
    result = np.array([value for _name, value in sorted(features.items())], dtype=np.float32)
    return result


def sample_ann():
    session = default_session()
    data = None
    expert_marks = []
    evaluations = session.query(ExpertEvaluation).options(sqlalchemy.orm.joinedload(ExpertEvaluation.docfragment))
    for e in evaluations:
        doc = Document(e.docfragment.forumpage.url)
        features = features_to_np(compute_features(doc.content()))
        #print(e.docfragment_id, features, e.evaluation, e.docfragment.evaluation)
        expert_marks.append(e.evaluation)
        if data is None:
            data = features
        else:
            data = np.vstack((data, features))
    nsamples, nfeatures = data.shape
    labels = np.array(list(expert_marks), dtype=np.float32)

    # Выделяем случайное подмножество для тестирования
    ntests = int(np.ceil(nsamples/10.0))
    samples_for_testing = np.random.choice(nsamples, ntests)

    test_data = data[np.ix_(samples_for_testing, np.arange(nfeatures))]
    test_labels = labels[np.ix_(samples_for_testing)]

    model = Sequential()
    model.add(Dense(32, activation='relu', input_dim=nfeatures))
    model.add(Dropout(0.5))
    model.add(Dense(62, activation='relu'))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(1, activation='softmax'))

    model.compile(optimizer='rmsprop',
                  loss='mean_squared_error', # 'categorical_crossentropy',
                  metrics=['accuracy'])

    ## Convert labels to categorical one-hot encoding
    # one_hot_labels = keras.utils.to_categorical(labels, num_classes=4)
    # test_one_hot_labels = keras.utils.to_categorical(test_labels, num_classes=4)
    one_hot_labels = labels
    test_one_hot_labels = test_labels

    # Train the model, iterating on the data in batches of 32 samples
    model.fit(data, one_hot_labels, epochs=10, batch_size=32)

    loss_and_metrics = model.evaluate(test_data, test_one_hot_labels, batch_size=128)
    model.save(settings.top_directory + '/local/model.keras')
    #print(loss_and_metrics)
    classes = model.predict(test_data, batch_size=128)
    print(classes, test_labels)
    return model


    # classes = model.predict(x_test, batch_size=128)


from keras.layers import Embedding
from keras.layers import LSTM
from keras.layers import Activation
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

MAX_SEQUENCE_LENGTH = 20
DICTIONARY_SIZE = 10000
EMBEDDING_DIM = 20

tokenizer = Tokenizer(num_words=DICTIONARY_SIZE, filters='#$%&()*+-/:;<=>@[\\]^{|}~\t\n,.!"')

# инициализируем слой эмбеддингов
NAME = "modified_lstm"

# выберем только первые 10000 слов из нашего словаря
first_10000 = {k: v for k, v in tokenizer.word_index.items() if v < 10000}

# загрузим вектора эмбеддингов
embeddings = {}
with open("EMBEDDINGS_FILE") as f:
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings[word] = coefs
    del values, word, coefs, line
print("Количество слов в векторном представлении:", len(embeddings))

# подготовим матрицу эмбеддингов, где по строкам будут индексы слов

embedding_matrix = np.zeros((tokenizer.num_words, EMBEDDING_DIM))
for word, i in first_10000.items():
    embedding_vector = embeddings.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector


embedding_layer = Embedding(tokenizer.num_words,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=False,
                            mask_zero=True)
                            
model = Sequential()
model.add(embedding_layer)
model.add(Dropout(0.2))
model.add(LSTM(100, dropout=0.1, recurrent_dropout=0.1))
model.add(Dropout(0.2))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
