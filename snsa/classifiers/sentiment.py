"""
    Классификатор оценки тональности отзыва.
"""
import sqlalchemy
from collections import namedtuple

import pickle
import keras
from keras.preprocessing.sequence import pad_sequences

import snsa.settings as settings

MAX_SEQUENCE_LENGTH=100 # Fixme: extract from loaded model!
TOKENIZER_FILENAME = settings.top_directory + settings.config.get('Classifiers', 'Tokenizer')
MODEL_FILENAME = settings.top_directory + settings.config.get('Classifiers', 'Opinion')

tokenizer = None
model = None

# loading
def _load_model():
    global tokenizer, model
    with open(TOKENIZER_FILENAME, 'rb') as handle:
        tokenizer = pickle.load(handle)
    model = keras.models.load_model(MODEL_FILENAME)

def is_a_review(text, threshold=0.6):
    global tokenizer, model

    if model is None:
        _load_model()
    sequences = tokenizer.texts_to_sequences([text])
    padded = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)
    prediction = model.predict([padded], batch_size=1)
    confidence = prediction[0, 0]
    return confidence > threshold, confidence

# import pandas as pd
# import numpy as np

# import gensim
# from gensim.models.word2vec import Word2Vec # the word2vec model gensim class
# LabeledSentence = gensim.models.doc2vec.LabeledSentence

# from sklearn.model_selection import train_test_split

# import keras
# from keras.models import Sequential
# from keras.layers import Dense, Dropout, LSTM, Embedding, Activation
# from keras.preprocessing.text import Tokenizer
# from keras.preprocessing.sequence import pad_sequences

# import snsa.settings as settings
# from snsa.models.opinion import ExpertEvaluation
# from snsa.utils.sqlalchemy import default_session

# from snsa.indexer.preprocessor import Document


# DataItem = namedtuple('DataItem', ['label', 'text', 'tokens'])

# def load_data(n_documents=1000):
#     from snsa.utils.sqlalchemy import DBSession
#     from snsa.models.explorer import ForumPage

#     texts = []
#     with DBSession() as session:
#         query = session.query(ForumPage.title).limit(n_documents).all()
#         for seq, row in enumerate(query):
#             (title,) = row
#             texts.append(DataItem(1, title, []))

#     return pd.DataFrame.from_records(
#         texts,
#         columns = ['label', 'text', 'tokens'])



# def tokenize(text):
#     from snsa.nlp.morph import morph
#     #tokenizer = Tokenizer(num_words=100, filters='''#$%&()*+-/:;<=>@[\\]^{|}~\t\n,.!"''')
#     tokens = keras.preprocessing.text.text_to_word_sequence(text)
#     tokens = list(filter(lambda tok: not morph.is_stopword(tok), tokens))
#     return tokens


# def train_word2vec(data):
#     size = 600
#     n = None
#     x_train, x_test, y_train, y_test = \
#         train_test_split(np.array(data.head(n).tokens),
#                          np.array(data.head(n).label),
#                          test_size=0.2)
#     w2v = Word2Vec(size=size, min_count=10)
#     w2v.build_vocab(x_train)
#     w2v.train(x_train, total_examples=w2v.corpus_count, epochs=w2v.epochs)
#     print(w2v.most_similar('зомета', topn=10))
#     print(w2v.most_similar('рмж', topn=10))
#     return w2v

# def dotest():
#     n_documents = 1000
#     data = load_data(n_documents=n_documents)
#     data['tokens'] = data['text'].map(tokenize)
#     model = train_word2vec(data)

#     return model



# # MAX_SEQUENCE_LENGTH = 20
# # DICTIONARY_SIZE = 10000
# # EMBEDDING_DIM = 20

# # tokenizer = Tokenizer(num_words=DICTIONARY_SIZE, filters='''#$%&()*+-/:;<=>@[\\]^{|}~\t\n,.!"''')

# # # инициализируем слой эмбеддингов
# # NAME = "modified_lstm"

# # # выберем только первые 10000 слов из нашего словаря
# # first_10000 = {k: v for k, v in tokenizer.word_index.items() if v < 10000}

# # # загрузим вектора эмбеддингов
# # embeddings = {}
# # with open("EMBEDDINGS_FILE") as f:
# #     for line in f:
# #         values = line.split()
# #         word = values[0]
# #         coefs = np.asarray(values[1:], dtype='float32')
# #         embeddings[word] = coefs
# #     del values, word, coefs, line
# # print("Количество слов в векторном представлении:", len(embeddings))

# # # подготовим матрицу эмбеддингов, где по строкам будут индексы слов

# # embedding_matrix = np.zeros((tokenizer.num_words, EMBEDDING_DIM))
# # for word, i in first_10000.items():
# #     embedding_vector = embeddings.get(word)
# #     if embedding_vector is not None:
# #         embedding_matrix[i] = embedding_vector


# # embedding_layer = Embedding(tokenizer.num_words,
# #                             EMBEDDING_DIM,
# #                             weights=[embedding_matrix],
# #                             input_length=MAX_SEQUENCE_LENGTH,
# #                             trainable=False,
# #                             mask_zero=True)
                            
# # model = Sequential()
# # model.add(embedding_layer)
# # model.add(Dropout(0.2))
# # model.add(LSTM(100, dropout=0.1, recurrent_dropout=0.1))
# # model.add(Dropout(0.2))
# # model.add(Dense(1))
# # model.add(Activation('sigmoid'))

# # model.compile(loss='binary_crossentropy',
# #               optimizer='adam',
# #               metrics=['accuracy'])
