# -*- coding: utf-8; -*-
# pylint: disable=unreachable
# cSpell:ignore snsa

from pprint import pprint

import snsa
from snsa import settings
from snsa.spider import yandex as ya

from snsa.models.explorer import ForumPage, SiteInfo
from snsa.models.tasks import ProcessingTask, TaskLog

from snsa.ontology.human import terms_for_concept
from snsa.nlp.features import compute_features

import pymorphy2
from pymystem3 import Mystem
from snsa.nlp.tokenizer import tokenize

class QuickTest(object):
    tests = {}
    def add_test(self, testname, fun):
        self.tests[testname] = fun

    def __call__(self, testname):
        fun = self.tests.get(testname)
        if fun is not None:
            fun()
        else:
            print("\nTest '%s' not found!\n" % testname)

qt = QuickTest()


def test_rdflib():
    import rdflib
    from rdflib.plugins.sparql import prepareQuery
    from snsa import settings
    g = rdflib.Graph()
    g.load(settings.top_directory + "/resources/drugs.rdf", format="turtle")
    for subj, prop, obj in g:
        print(subj, prop, obj)
    # FILTER (?label = "Zometa"^^xsd:string) .
    if not hasattr(test_rdflib, 'query'):
        test_rdflib.query = prepareQuery(
            """SELECT DISTINCT ?label ?name
            WHERE {
              ?d rdf:label ?label .
              ?d do:name ?name .
            }""",
            initNs=dict(g.namespaces()))
    qres = g.query(test_rdflib.query,
            initBindings={'label': rdflib.Literal('Zometa'),})
    for row in qres:
        print("'%s' is '%s'" % row)


morph = pymorphy2.MorphAnalyzer()
mystem = Mystem()

def find_verbs(sentence):
    from snsa.nlp.tokenizer import tokenize
    for word in tokenize(sentence):
        for p in morph.parse(word):
            if p.score > 0.3 and p.tag.POS == 'VERB':
                print(word, p.normal_form)

def find_verbs2(text):
    # https://tech.yandex.ru/mystem/doc/grammemes-values-docpage/
    stemmed = mystem.analyze(text)
    for item in stemmed:
        word_analysis = item.get('analysis')
        if word_analysis and  word_analysis[0]['gr'].startswith('S'):
            print(item['text'], [(wa_variant['gr'], wa_variant['lex']) for wa_variant in word_analysis])
    #print(stemmed)
    return True

def test_tokenizer():
    from snsa.nlp.tokenizer import tokenize
    sentence = 'ее'
    words = tokenize(sentence)
    print(sentence)
    for word in words:
        print([(p.normal_form, p.tag, p.methods_stack) for p in morph.parse(word) if p.score > 0.2])

        #stemmed = mystem.analyze(word)
        #print(stemmed)
        print(morph.parse(word))

    print(compute_features(sentence))
    return

    #week = morph.parse('гомоморфизище')[0]
    #for form in week.lexeme:
    #    print(form)
    return



def contain_words(text, words):
    words_lower = [w.lower() for w in words]
    for token in tokenize(text):
        for p in morph.parse(token):
            if p.normal_form.lower() in words_lower:
                return True
    return False


def test_lexicon():
    from snsa.nlp.structure import Passage
    from snsa.nlp.lexicon import EvaluationLexicon, LexiconLocator
    from snsa.nlp.features import IMPROVERS, MORE_LESS

    imp_lex = EvaluationLexicon('improvers', IMPROVERS)

    lex = EvaluationLexicon('more/less', MORE_LESS, improvers=imp_lex)
    p = Passage('Я узнала, что есть препараты Зомета и Бондронат, которые гораздо чаще оказываются эффективнее бонефоса.')
    loc = LexiconLocator(lex)
    pprint(list(map(lambda obj: obj.__str__(), loc.locate(p))))


def test_features():
    from snsa.nlp.features import compute_features
    texts = ['''Такую жутко неприятную болячку как язвенный колит, я заработала года 3 назад. Чего только мы с моим врачом не перепробовали, чтобы от нее избавиться.
Толку чуть. На последнем приеме выписали мне новые таблетки под названием Мезавант. Доктор предупредил что они очень дорогие, но очень действенные.
Купила, потому что мучиться дальше сил уже не было. Доза, которую мне назначили, была 2 таблетки в день. Через неделю приема симптомы колита стали
понемногу отступать, а через месяц приема я почувствовала себя счастливой женщиной. У меня ничего не болело и это было классно! Эффект этого лекарства
настолько суперский, что перечеркивает один его недостаток - высокую цену.''',
    '''Как и многие лекарственные средства препарат не лишен побочных эффектов, но по сравнению с другими месалазиносодержащими препаратами побочек
    не так уж и много. На себе побочных эффектов не обнаружила вообще. Гастроэнтеролог назначила мне прием по 2 таблетки Мезаванта (2,4г месалазина)
    один раз в день во время еды. Через 3 дня лечения мне стало гораздо лучше, а через 5 дней прошли все симптомы язвенного колита, я наконец почувствовала
    себя здоровым человеком! Через пару месяцев приема я снизила дозу препарата до одной таблетки в день. Чувствую себя хорошо, по результатам лечения была
    достигнута ремиссия. ''',
    '''Таблетки (кишечнорастворимые) начинают растворяться в тонкой кишке (при pH >6) через 110–170 мин, полностью растворяются через 165–225 мин после приема. Во время пассажа по кишечнику месалазин высвобождается из таблеток постепенно, причем 15–30% — в подвздошной кишке, 60–75% — в толстой кишке, в кровь попадает лишь 10%. Концентрация в плазме низкая — после однократного приема 250 мг Cmax составляет 0,5–1,5 мкг/мл. В слизистой оболочке кишечника и печени образует N-ацетил-5-аминосалициловую кислоту. Связывание месалазина с белками плазмы около 40%, метаболита — 75–83%.''',
    '''Сегодня был на приёме у гастроэнтеролога, сказала, что недавно была на семинаре каком-то на котором говорили о новом препарате, называется МЕЗАВЕНТ, форма выпуска-гранулы. В России должен появиться через месяцев пять в продаже.Данный препарат будет крайне полезен тем, у кого поражен гистальный отдел кишечника, т.к. таблетки и свечи при данном поражении недостаточно действенны, т.к. попросту не доходят до этого отдела кишечника. По её заверениям активность данного препарата очень высока и одного приёма в день достаточно для поддержания нормального состояния. Только вот не нашёл ничего в интернете по этому препарату( Только на иностранных сайтах есть скудная информация (MEZAVENT если в гугл вбить), которая по сути ни о чём не говорит ((( так что будем ждать. > > P.S. может кто знает о нём что-то подробнее,- отпишитесь, может это я как- то коряво искал, что ничего не нашёл) ''',
    ]
    for seq, text in enumerate(texts):
        features = compute_features(text, None)
        print(seq, features)


def test_explore():
    from snsa.explorer import popular_collocations
    #return popular_collocations(['бонефос'])
    popular_collocations(['зомета'])

def test_feapi():
    from snsa.analyzer import analyze
    from snsa.feapi.pages import page_data
    analyze(1971)
    #print(page_data(2508))


def test_ann():
    from snsa.classifiers.sentiment import dotest
    print(dotest())

def test_toks():
    from snsa.nlp.tokenizer import find_paragraphs
    ps = find_paragraphs('abc')
    text = '''

    dfkdfsddf

 
        sdfsfd

    sdfsdfdsfs'''
    for (seq, p) in enumerate(find_paragraphs(text)):
        print(seq, p)


qt.add_test('lexicon', test_lexicon)
qt.add_test('explore', test_explore)
qt.add_test('features', test_features)
qt.add_test('feapi', test_feapi)
qt.add_test('ann', test_ann)
qt.add_test('toks', test_toks)


def quicktest(testname=None):
    qt(testname)
    return

    from snsa.nlp.topics import build_topic_model
    build_topic_model('Зомета')
    return

    from snsa.explorer import popular_collocations
    print(popular_collocations(['зомета']))
    return

    import snsa.nlp.lexicon as lex
    print(lex.find_main_word('красная кожа'), end='\n========================================\n')
    print(lex.find_main_word('кожа красная'), end='\n========================================\n')
    print(lex.find_main_word('метод Иванова'), end='\n========================================\n')

    for p in morph.parse('зометы'):
        print(p)
    return

    from snsa.explorer import process_tasks
    process_tasks()

    #yandex = ya.default_engine
    #print(yandex.get_limits())
    return

    #create_schema()

    from snsa.utils.sqlalchemy import DBSession
    with DBSession() as session:
        query = session.query(ProcessingTask).filter(ProcessingTask.id == 1)
        the_task = query.first()
        if the_task:
            the_task.update_status('updated 2')
    return

    #test_tokenizer()
    #return

    from snsa.explorer import urls_for_drugname
    from snsa.indexer.preprocessor import Document
    from snsa.nlp.tokenizer import find_paragraphs, normalize
    for seq, url in enumerate(urls_for_drugname('Zometa')):
        if seq < 100:
            continue
        doc = Document(url, autosave=True)
        for paragraph in find_paragraphs(doc.content()):
            if not contain_words(paragraph, ['Зомета']):
                continue
            normalized = normalize(paragraph)
            if(len(normalized) > 50 and len(normalized) > 0):
                print('"%s"' % normalized, end='\n')
                # find_verbs2(normalized)
                print(compute_features(normalized), end='\n\n')
        break
    #test_tokenizer()

    return
    return test_rdflib()
