Руководство по установке
========================

Установка под Linux
-------------------

Ставим необходимые системные пакеты:

.. code-block:: bash

  sudo apt install python3 python3-pip
  sudo apt install python3-pip
  pip3 install --upgrade pip
  pip3 install virtualenv
  python3 -m  virtualenv env
  sudo apt install virtualenvwrapper

Создаем и активируем виртуальное окружение

.. code-block:: bash

  . /usr/share/virtualenvwrapper/virtualenvwrapper.sh
  mkvirtualenv snsa
  workon snsa
  cdvirtualenv

Скачиваем исходный код из репозитария и устанавливаем в виртуальное окружение необходимые пакеты:

.. code-block:: bash

  git clone git@gitlab.com:sergadin/snsa.git
  cd snsa
  pip3 install -r requirements.txt

Начальная установка для Django:

.. code-block:: bash

  cd snsa_frontend
  python3 manage.py migrate


Запускаем локальный сервер:

.. code-block:: bash

  python3 manage.py runserver


После этого можно открыть страницу http://localhost:8000/
