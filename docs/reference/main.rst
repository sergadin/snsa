Руководство программиста
========================

ontology
--------

.. automodule:: snsa.ontology
   :members:
   :undoc-members:

.. automodule:: snsa.ontology.human
   :members:


.. automodule:: snsa.indexer.preprocessor
   :members:

NLP
---

.. automodule:: snsa.nlp
   :members:

.. automodule:: snsa.nlp.morph
   :members:

.. automodule:: snsa.nlp.tokenizer
   :members:


Утилиты
-------

.. autoclass:: snsa.explorer.Explorer
   :members:
