# -*- coding: utf-8; -*-
# pylint: disable=unreachable

import argparse

import snsa
from snsa import settings
from snsa.spider import yandex as ya
import snsa.models

def create_schema():
    settings.DB.Base.metadata.create_all(bind=settings.DB.engine)


def get_parser():
    """Создает анализатор аргументов командной строки."""
    parser = argparse.ArgumentParser(prog='snsa', description='Command line utility for SNSA.')
    version = '%(prog)s ' + snsa.__version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--createSchema', '-s', action='store_true',
                        help='Создает схему базы данных', )
    parser.add_argument('--yandexLimits', action='store_true',
                        help='Проверить оставшееся количество запросов к Yandex', )
    parser.add_argument('--taskid', type=int,
                        help='Номер задания, которое необходимо обработать.', )
    parser.add_argument('--operate', '-o', type=str,
                        choices=['explore_all', 'explore', 'download', 'analyze', 'clear'],
                        help='Действие, которое необходимо выполнить для задания.')
    parser.add_argument('--addTask', '-a', type=str, metavar='DRUGNAME',
                        help='Добавление задания на обработку препарата DRUGNAME.')

    parser.add_argument('--doTest', type=str, metavar='TESTNAME',
                        help='Запуск теста в процесе разработки.')
    return parser


def main(args=None):
    print("Starting...")
    parser = get_parser()
    args = parser.parse_args(args)
    args = vars(args)

    if args['doTest']:
        from quicktest import quicktest
        return quicktest(args['doTest'])

    if (args['operate'] is not None and args['operate'] != 'explore_all') and args['taskid'] is None:
        print("No taskid specified!")
        return

    if args['createSchema']:
        create_schema()
    if args['operate'] == 'explore_all':
        from snsa.explorer import process_all_tasks
        process_all_tasks()
    if args['operate'] == 'explore':
        from snsa.explorer import process_tasks
        process_tasks()
    if args['operate'] == 'download':
        from snsa.explorer import download_pages
        download_pages(args['taskid'])
    if args['operate'] == 'analyze':
        from snsa.explorer import analyze_pages
        analyze_pages(args['taskid'])

    if args['yandexLimits']:
        yandex = ya.default_engine
        print(yandex.get_limits())

    return

if __name__ == "__main__":
    # execute only if run as a script
    main()
